import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AppState } from '@app/store/app.reducer';
import {
  newCollection,
  updateCollection,
} from '@collection/store/collections.actions';
import { Collection } from '@collection/store/collections.reducer';
import { Store } from '@ngrx/store';
import collectionTheme, { Color } from '@shared/collection-theme';

export type Mode = 'CREATE' | 'UPDATE';

const COLORS_INITIAL: Color[] = [
  { name: 'Default', code: collectionTheme.default.background, selected: true },
  { name: 'Red', code: collectionTheme.red.background, selected: false },
  { name: 'Blue', code: collectionTheme.blue.background, selected: false },
];

@Component({
  selector: 'app-collection-dialog',
  templateUrl: './collection-dialog.component.html',
  styleUrls: ['./collection-dialog.component.scss'],
})
export class CollectionDialogComponent implements OnInit {
  mode: Mode;
  collectionForm: FormGroup;
  colors = COLORS_INITIAL;
  collection: Collection;

  constructor(
    private dialogRef: MatDialogRef<CollectionDialogComponent>,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.collectionForm = new FormGroup({
      collectionName: new FormControl(
        this.collection ? this.collection.name : '',
        [Validators.required]
      ),
    });
    if (this.collection) {
      this.colors = this.colors.map((color) => ({
        ...color,
        selected: this.collection.colorCode === color.code,
      }));
    }
  }

  onSubmit() {
    const colorCode = this.colors.find((collection) => collection.selected)
      .code;
    if (this.mode === 'CREATE') {
      this.store.dispatch(
        newCollection({
          name: this.collectionForm.get('collectionName').value,
          colorCode,
        })
      );
    } else {
      this.store.dispatch(
        updateCollection({
          collectionId: this.collection._id,
          update: {
            name: this.collectionForm.get('collectionName').value,
            colorCode,
          },
        })
      );
    }
    this.dialogRef.close({ ...this.collectionForm.value, colorCode });
  }

  onColorSelect(colorId: number) {
    this.colors = this.colors.map((color) => ({ ...color, selected: false }));
    this.colors[colorId].selected = true;
  }
}
