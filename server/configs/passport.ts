import path from 'path';
require('dotenv').config({ path: path.join(__dirname, '../../.env') });
import passport from 'passport';
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import { Strategy as LocalStrategy } from 'passport-local';

import Users, { User } from '../models/user';

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID as string,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET as string,
      callbackURL: '/api/auth/google/callback',
    },
    async (accessToken, refreshToken, profile, done) => {
      if (profile) {
        try {
          const user = await Users.findOne({ googleID: profile.id });
          if (!user) {
            if (
              profile.emails &&
              profile.photos &&
              (await Users.validateOAuthId('google', profile.id))
            ) {
              const newUser = await new Users({
                email: profile.emails[0].value,
                googleID: profile.id,
                displayName: profile.displayName,
                avatar: profile.photos[0].value,
                provider: profile.provider,
              }).save();
              return done(null, newUser);
            } else {
              return done(null, false, { message: 'USER_ALREADY_EXISTED' });
            }
          } else {
            return done(null, user);
          }
        } catch (err) {
          return done(null, false, { message: 'ERROR_DATABASE' });
        }
      }
      return done(null, false, { message: 'ERROR_AUTHENTICATION' });
    }
  )
);

passport.use(
  new FacebookStrategy(
    {
      clientID: process.env.FACEBOOK_APP_ID as string,
      clientSecret: process.env.FACEBOOK_APP_SECRET as string,
      callbackURL: '/api/auth/facebook/callback',
    },
    async (accessToken, refreshToken, profile, done) => {
      if (profile) {
        try {
          const user = await Users.findOne({ facebookID: profile.id });
          if (!user) {
            if (await Users.validateOAuthId('facebook', profile.id)) {
              const newUser = await new Users({
                facebookID: profile.id,
                displayName: profile.displayName,
                provider: 'facebook',
              }).save();
              return done(null, newUser);
            } else {
              return done(null, false, { message: 'USER_ALREADY_EXISTED' });
            }
          } else {
            return done(null, user);
          }
        } catch (err) {
          done(null, false, { message: 'ERROR_DATABASE' });
        }
      }
      return done(null, false, { message: 'ERROR_AUTHENTICATION' });
    }
  )
);

passport.use(
  new LocalStrategy(
    {
      usernameField: 'identifier',
      passReqToCallback: true,
    },
    async (req, identifier, password, done) => {
      try {
        const user = await Users.findOne({
          $or: [{ username: identifier }, { email: identifier }],
          provider: 'local',
        });

        if (user) {
          if (await user.comparePassword(password)) {
            return done(null, user);
          } else {
            return done(null, false, { message: 'INCORRECT_PASSWORD' });
          }
        }
        return done(null, false, { message: 'USER_NOT_FOUND' });
      } catch (err) {
        return done(null, false, { message: 'ERROR_DATABASE' });
      }
    }
  )
);

passport.serializeUser((user: User, done) => {
  done(null, user._id);
});

passport.deserializeUser((id, done) => {
  Users.findById(id, (err, user) => {
    if (err) {
      done(err);
    } else if (!user) {
      done(null, false);
    } else {
      done(null, user);
    }
  });
});

export default passport;
