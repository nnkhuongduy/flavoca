import { lowerCase } from 'lodash';

export const convertCollectionName = (collectionName: string): string => {
  return lowerCase(collectionName).replace(/ /g, '');
};
