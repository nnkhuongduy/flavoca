import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

import { AppState } from '@app/store/app.reducer';
import { selectCollections } from '@app/store/app.selectors';
import { fetchCollections } from '@app/store/app.actions';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
})
export class CollectionsComponent implements OnInit {
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store
      .select(selectCollections)
      .pipe(take(1))
      .subscribe((collections) => {
        if (!collections.length) {
          this.store.dispatch(fetchCollections({ loading: 'collections' }));
        }
      });
  }
}
