import { Component, Inject } from '@angular/core';
import {
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA,
} from '@angular/material/snack-bar';

export type SnackbarSeverity = 'error' | 'warning' | 'information' | 'success';

export interface SnackbarData {
  message: string;
  severity: SnackbarSeverity;
}

@Component({
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss'],
})
export class SnackbarComponent {
  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: SnackbarData,
    private snackbarRef: MatSnackBarRef<SnackbarComponent>
  ) {}

  onDismiss() {
    this.snackbarRef.dismiss();
  }
}
