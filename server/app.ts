import app from './configs/express';
import { mongooseConnect } from './configs/mongoose';
import CONFIGS from './configs/configs';

mongooseConnect();

app.listen(CONFIGS.PORT, () => {
  console.log(`Server started on port ${CONFIGS.PORT}`);
});
