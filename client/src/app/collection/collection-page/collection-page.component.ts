import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject, combineLatest } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';

import { Collection } from '@collection/store/collections.reducer';
import { AppState } from '@app/store/app.reducer';
import { selectCollections } from '@app/store/app.selectors';
import { selectIsLoading } from '@collection/store/collections.selectors';

@Component({
  selector: 'app-collection-page',
  templateUrl: './collection-page.component.html',
  styleUrls: ['./collection-page.component.scss'],
})
export class CollectionPageComponent implements OnInit, OnDestroy {
  private ngOnDestroy$ = new Subject();
  collection: Collection;
  isLoading = false;

  constructor(private route: ActivatedRoute, private store: Store<AppState>) {}

  ngOnInit(): void {
    combineLatest([
      this.route.params,
      this.store.select(selectCollections),
      this.store.select(selectIsLoading),
    ])
      .pipe(
        takeUntil(this.ngOnDestroy$),
        map(([params, collections, loading]) => ({
          collectionName: params.collectionName,
          collections,
          loading,
        }))
      )
      .subscribe(({ collectionName, collections, loading }) => {
        if (collectionName && collections) {
          this.collection = collections.find(
            (collectionEle) =>
              collectionEle.name.replace(/ /g, '').toLowerCase() ===
              collectionName
          );
        } else {
          this.collection = null;
        }
        this.isLoading = loading;
      });
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }
}
