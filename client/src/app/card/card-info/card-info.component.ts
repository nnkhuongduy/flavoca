import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject, combineLatest } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { DropdownDirective } from '@shared/directives/dropdown.directive';
import { CardCollectionDropdownComponent } from './collection-dropdown/collection-dropdown.component';
import { CardService, ViewType, View } from '@card/card.service';
import { AppState } from '@app/store/app.reducer';
import { selectLoadingState } from '@card/store/card.selectors';
import { UtilitiesService } from '@shared/services/utilities.service';
import {
  selectCardInDic,
  selectCardsDic,
  selectCollections,
} from '@app/store/app.selectors';
import { fetchCollections } from '@app/store/app.actions';
import { Collection } from '@collection/store/collections.reducer';
import { CardInfo } from '@card/store/card.reducer';

@Component({
  selector: 'app-card-info',
  templateUrl: './card-info.component.html',
  styleUrls: ['./card-info.component.scss'],
})
export class CardInfoComponent implements OnInit, OnDestroy {
  @ViewChild(DropdownDirective) collectionHost: DropdownDirective;
  @ViewChild('dropdown') dropdownContainer: ElementRef;
  private ngOnDestroy$ = new Subject();
  private collectionDropdownRef: ComponentRef<CardCollectionDropdownComponent>;
  viewType: ViewType;
  cardForm: FormGroup;
  currentView: View;
  preview: boolean;
  loading: boolean;
  selectedCard: CardInfo;
  collections: Collection[];

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private store: Store<AppState>,
    private cardService: CardService,
    private utilitiesService: UtilitiesService
  ) {}

  ngOnInit(): void {
    this.cardForm = this.cardService.cardForm;
    combineLatest([
      this.cardService.viewType,
      this.cardService.currentView,
      this.cardService.preview,
      this.cardService.selectedId,
      this.store.select(selectLoadingState, { loadingType: 'post' }),
      this.store.select(selectCollections),
      this.store.select(selectCardsDic),
    ])
      .pipe(
        takeUntil(this.ngOnDestroy$),
        map(
          ([
            viewType,
            currentView,
            preview,
            selectedId,
            loading,
            collections,
            cardDic,
          ]) => ({
            viewType,
            currentView,
            preview,
            selectedId,
            loading,
            collections,
            cardDic,
          })
        )
      )
      .subscribe(
        ({
          viewType,
          currentView,
          preview,
          loading,
          selectedId,
          collections,
          cardDic,
        }) => {
          this.viewType = viewType;
          this.preview = preview;
          this.currentView = currentView;
          this.loading = loading;
          if (!collections) {
            this.store.dispatch(fetchCollections({ loading: 'cards' }));
          } else {
            this.collections = collections;
          }
          if (selectedId && cardDic[selectedId]) {
            this.selectedCard = cardDic[selectedId];
          } else {
            this.selectedCard = null;
          }
        }
      );
    this.utilitiesService.documentClickedTarget
      .pipe(takeUntil(this.ngOnDestroy$))
      .subscribe((target) => this.documentClickListener(target));
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onView() {
    this.cardService.toggleViewType();
  }

  onFlip() {
    this.cardService.toggleCurrentView();
  }

  onPreview() {
    this.cardService.togglePreview();
  }

  onRepeatSound() {
    const audio = new Audio(this.cardForm.get('audio').value);
    audio.load();
    audio.play();
  }

  async onClear() {
    if (await this.cardService.checkDirty()) {
      this.cardService.clearForm();
    }
  }

  onSwitchCollection() {
    const viewContainerRef = this.collectionHost.viewContainerRef;
    viewContainerRef.clear();
    if (!this.collectionDropdownRef) {
      const collectionDropdownResolver = this.componentFactoryResolver.resolveComponentFactory(
        CardCollectionDropdownComponent
      );
      this.collectionDropdownRef = viewContainerRef.createComponent(
        collectionDropdownResolver
      );
      this.collectionDropdownRef.instance.selected.subscribe(
        (collectionId: string) => {
          this.onSwitchCollection();
          this.cardService.patchForm({
            collectionId,
          });
        }
      );
    } else {
      this.collectionDropdownRef.instance.selected.unsubscribe();
      this.collectionDropdownRef.destroy();
      this.collectionDropdownRef = null;
    }
  }

  onSave() {
    this.cardService.save();
  }

  onDelete() {
    this.cardService.deleteCard();
  }

  documentClickListener(target: HTMLElement) {
    if (this.collectionDropdownRef && this.dropdownContainer) {
      if (!this.dropdownContainer.nativeElement.contains(target)) {
        this.onSwitchCollection();
      }
    }
  }

  async onRemove() {
    if (await this.cardService.checkDirty()) {
      this.cardService.removeSelected();
    }
  }

  getCollectionName(id: string) {
    if (this.collections) {
      const collection = this.collections.find(
        (collectionEle) => collectionEle._id === id
      );
      return collection ? collection.name : '';
    }
    return '';
  }

  get audioControl() {
    if (this.cardForm) {
      return this.cardForm.get('audio');
    }
  }

  get cardName() {
    return this.selectedCard
      ? this.selectedCard.front
      : this.cardForm.get('front').value;
  }
}
