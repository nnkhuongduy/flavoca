import { User, UserClient } from '../models/user';

export const getUserClient = ({
  username,
  email,
  avatar,
  displayName,
  provider,
  verified,
  createdAt,
  updatedAt,
}: User): UserClient => ({
  username,
  email,
  avatar,
  displayName,
  provider,
  verified,
  createdAt,
  updatedAt,
});
