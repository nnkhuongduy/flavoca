import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '@app/store/app.reducer';
import { oAuthStart } from '@auth/store/auth.actions';

@Component({
  template: ``,
})
export class OAuthComponent implements OnInit {
  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.store.dispatch(oAuthStart());
  }
}
