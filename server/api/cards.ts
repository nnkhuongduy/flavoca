import { Router } from 'express';

import { isAuthenticated } from '../middlewares/authenticated';
import {
  postCard,
  getCards,
  cardActions,
  patchCard,
  getCard,
} from '../service/card';

const cardsRouter = Router();

cardsRouter
  .post('/cards', isAuthenticated, postCard, getCards)
  .get('/cards', isAuthenticated, getCards)
  .get('/cards/:cardId', isAuthenticated, getCard)
  .put('/cards', isAuthenticated, cardActions, getCards)
  .patch('/cards', isAuthenticated, patchCard, getCards);

export default cardsRouter;
