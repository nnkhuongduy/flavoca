import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { CardService, View, ViewType } from '@card/card.service';
import { CardInfo } from '@card/store/card.reducer';
import { combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-card-active',
  templateUrl: './card-active.component.html',
  styleUrls: ['./card-active.component.scss'],
})
export class CardActiveComponent implements OnInit, OnDestroy {
  @Input() state: View;
  private ngOnDestroy$ = new Subject();
  frontInput: string;
  card: CardInfo;
  currentView: View;
  viewType: ViewType;

  constructor(private cardService: CardService) {}

  ngOnInit(): void {
    this.card = this.cardService.cardForm.value;
    combineLatest([
      this.cardService.currentView,
      this.cardService.viewType,
      this.cardService.frontInput,
    ])
      .pipe(
        takeUntil(this.ngOnDestroy$),
        map(([currentView, viewType, frontInput]) => ({
          currentView,
          viewType,
          frontInput,
        }))
      )
      .subscribe(({ currentView, viewType, frontInput }) => {
        this.currentView = currentView;
        this.viewType = viewType;
        this.frontInput = frontInput;
      });
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onKey(event: InputEvent) {
    const value = (event.currentTarget as HTMLInputElement).value;
    this.cardService.frontInput.next(value);
  }
}
