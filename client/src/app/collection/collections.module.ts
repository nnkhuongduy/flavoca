import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CollectionsComponent } from '@collection/collections.component';
import { CollectionsRoutingModule } from '@collection/collections-routing.module';
import { CollectionCreateComponent } from '@collection/collection-create/collection-create.component';
import * as fromCollections from '@collection/store/collections.reducer';
import { CollectionDialogComponent } from '@collection/collection-dialog/collection-dialog.component';
import { SharedModule } from '@app/shared/shared.module';
import { CollectionsEffect } from '@collection/store/collections.effects';
import { AuthGuard } from '@auth/auth-guard.service';
import { CollectionBoxComponent } from '@collection/collection-box/collection-box.component';
import { CollectionPageComponent } from '@collection/collection-page/collection-page.component';
import { CollectionInfoComponent } from '@collection/collection-page/collection-info/collection-info.component';
import { CollectionSettingDropdownComponent } from '@collection/collection-page/collection-info/setting-dropdow/setting-dropdow.component';
import { CollectionDeleteDialogComponent } from '@collection/collection-page/collection-info/delete-dialog/delete-dialog.component';
import { CollectionInteractionComponent } from '@collection/collection-page/collection-interaction/collection-interaction.component';
import { CollectionsDashboardComponent } from '@collection/collections-dashboard/collections-dashboard.component';

@NgModule({
  declarations: [
    CollectionsComponent,
    CollectionCreateComponent,
    CollectionDialogComponent,
    CollectionBoxComponent,
    CollectionPageComponent,
    CollectionInfoComponent,
    CollectionSettingDropdownComponent,
    CollectionDeleteDialogComponent,
    CollectionInteractionComponent,
    CollectionsDashboardComponent,
  ],
  imports: [
    CollectionsRoutingModule,
    StoreModule.forFeature(
      fromCollections.collectionsFeatureKey,
      fromCollections.reducer
    ),
    EffectsModule.forFeature([CollectionsEffect]),
    SharedModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [AuthGuard],
})
export class CollectionsModule {}
