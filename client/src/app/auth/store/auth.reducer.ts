import { createReducer, on, Action } from '@ngrx/store';
import {
  oAuthStart,
  authRequestFail,
  authRequestSuccess,
  setErrorMessage,
  signinStart,
  signupStart,
  updateStart,
} from '@auth/store/auth.actions';

export const authFeatureKey = 'auth';

export interface State {
  isSigning: boolean;
  errorMessage: string;
}

const initialState: State = {
  isSigning: false,
  errorMessage: null,
};

const authReducer = createReducer(
  initialState,
  on(signupStart, (state) => ({
    ...state,
    isSigning: true,
    errorMessage: null,
  })),
  on(signinStart, (state) => ({
    ...state,
    isSigning: true,
    errorMessage: null,
  })),
  on(setErrorMessage, (state, { message }) => ({
    ...state,
    errorMessage: message ? message : null,
  })),
  on(oAuthStart, (state) => ({
    ...state,
    isSigning: true,
    errorMessage: null,
  })),
  on(updateStart, (state) => ({
    ...state,
    isSigning: true,
    errorMessage: null,
  })),
  on(authRequestFail, (state, { error }) => ({
    ...state,
    isSigning: false,
    errorMessage: error,
  })),
  on(authRequestSuccess, (state) => ({
    ...state,
    isSigning: false,
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return authReducer(state, action);
}
