import { MatDialog } from '@angular/material/dialog';
import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest, Subject, Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map, take, takeLast } from 'rxjs/operators';

import { AppState } from '@app/store/app.reducer';
import { CardInfo } from '@card/store/card.reducer';
import { deleteCard, patchCard, postCard } from '@card/store/card.actions';
import {
  selectCardInDic,
  selectCards,
  selectCardsDic,
} from '@app/store/app.selectors';
import { CardConfirmationComponent } from '@card/confirmation/confirmation.component';
import {
  SnackbarComponent,
  SnackbarData,
} from '@shared/components/snackbar/snackbar.component';
import { fetchCard } from '@app/store/app.actions';

export type ViewType = 'flip' | 'side';
export type View = 'front' | 'back';
export type Media = 'image' | 'audio';

const DEFAULT_FORM: Omit<
  CardInfo,
  'createdAt' | 'updatedAt' | 'state' | '_id'
> = {
  front: '',
  definition: '',
  collectionId: '',
  addition: [],
  frontInput: false,
  image: '',
  audio: '',
};

@Injectable({
  providedIn: 'root',
})
export class CardService {
  private cardsDicSub: Subscription;
  selectedId = new BehaviorSubject<string>(null);
  cardForm = new FormGroup({
    front: new FormControl('', Validators.required),
    definition: new FormControl('', Validators.required),
    addition: new FormArray([]),
    collectionId: new FormControl('', Validators.required),
    frontInput: new FormControl(false),
    image: new FormControl('', [Validators.pattern(/^(http(s|):\/\/)/)]),
    audio: new FormControl('', [Validators.pattern(/^(http(s|):\/\/)/)]),
  });
  frontInput = new BehaviorSubject<string>('');
  viewType = new BehaviorSubject<ViewType>('side');
  currentView = new BehaviorSubject<View>('front');
  preview = new BehaviorSubject<boolean>(false);
  touched = new Subject<boolean>();
  edit = new BehaviorSubject<boolean>(false);
  collectionFilter = new Subject<string>();

  constructor(
    private store: Store<AppState>,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {}

  async checkDirty() {
    let result = true;
    const form = this.cardForm.value;
    const selectedCard: CardInfo = await this.store
      .select(selectCardInDic, { cardId: this.selectedId.value })
      .pipe(take(1))
      .toPromise();
    if (selectedCard) {
      Object.keys(selectedCard).forEach((key) => {
        if (form[key]) {
          if (key === 'addition') {
            if (form.addition.length !== selectedCard.addition.length) {
              result = false;
              return;
            }
            selectedCard.addition.forEach((add, index) => {
              if (form.addition[index] !== add) {
                result = false;
              }
            });
            return;
          }
          if (form[key] !== selectedCard[key]) {
            result = false;
          }
        }
      });
    } else {
      Object.keys(form).forEach((key) => {
        if (key === 'addition') {
          if (form.addition.length) {
            result = false;
          }
          return;
        }
        if (form[key] !== DEFAULT_FORM[key]) {
          result = false;
        }
      });
    }
    if (!result) {
      const dialogRef = this.dialog.open(CardConfirmationComponent, {
        minWidth: 400,
        data: { content: 'Do you want to discard the changes on this card?' },
      });
      result = await dialogRef.afterClosed().toPromise();
    }
    return result;
  }

  async setSelectedCard(cardId: string) {
    if (!this.cardsDicSub) {
      this.cardsDicSub = this.store.select(selectCardsDic).subscribe((dic) => {
        const cardInfo = dic[cardId];
        if (cardInfo) {
          this.clearForm();
          cardInfo.addition.forEach(() => this.addInput());
          this.cardForm.setValue({
            front: cardInfo.front,
            addition: cardInfo.addition,
            definition: cardInfo.definition,
            collectionId: cardInfo.collectionId,
            frontInput: cardInfo.frontInput,
            image: cardInfo.image,
            audio: cardInfo.audio,
          });
          this.selectedId.next(cardInfo._id);
        } else {
          this.store.dispatch(fetchCard({ cardId }));
        }
      });
    } else {
      if (await this.checkDirty()) {
        const oldSelectedId = this.selectedId.value;
        this.removeSelected();
        if (oldSelectedId !== cardId) {
          this.setSelectedCard(cardId);
        }
      }
    }
  }

  toggleViewType() {
    this.viewType.next(this.viewType.getValue() === 'flip' ? 'side' : 'flip');
  }

  toggleCurrentView() {
    this.currentView.next(
      this.currentView.getValue() === 'front' ? 'back' : 'front'
    );
  }

  togglePreview() {
    this.preview.next(!this.preview.getValue());
  }

  clearForm() {
    (this.cardForm.get('addition') as FormArray).clear();
    this.cardForm.setValue({
      front: '',
      definition: '',
      addition: [],
      frontInput: false,
      collectionId: '',
      image: '',
      audio: '',
    });
    this.frontInput.next('');
  }

  async patchForm(
    value: Partial<Omit<CardInfo, '_id' | 'state' | 'createdAt' | 'updatedAt'>>
  ) {
    this.cardForm.patchValue(value);
  }

  async save() {
    const selectedId = this.selectedId.value;
    const cardForm = this.cardForm;
    if (selectedId) {
      this.store.dispatch(
        patchCard({ cardId: selectedId, cardInfo: cardForm.value })
      );
    } else {
      this.store.dispatch(postCard({ card: cardForm.value }));
    }
  }

  addInput() {
    (this.cardForm.get('addition') as FormArray).push(
      new FormControl('', Validators.required)
    );
  }

  deleteInput(index: number) {
    (this.cardForm.get('addition') as FormArray).removeAt(index);
  }

  deleteCard() {
    const dialogRef = this.dialog.open(CardConfirmationComponent, {
      minWidth: 400,
      data: { content: 'Do you want to delete this card?' },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const cardId = this.selectedId.value;
        this.store.dispatch(deleteCard({ cardId }));
      }
    });
  }

  removeSelected() {
    this.selectedId.next(null);
    this.cardsDicSub.unsubscribe();
    this.cardsDicSub = undefined;
    this.clearForm();
  }

  showError(snackbar: SnackbarData) {
    this.snackbar.openFromComponent(SnackbarComponent, {
      data: snackbar,
      panelClass: ['snackbar', `snackbar-${snackbar.severity}`],
    });
  }
}
