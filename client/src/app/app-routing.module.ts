import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./home/home.module').then((mod) => mod.HomeModule),
        pathMatch: 'full',
      },
      {
        path: 'collections',
        loadChildren: () =>
          import('@collection/collections.module').then(
            (mod) => mod.CollectionsModule
          ),
      },
      {
        path: 'auth',
        loadChildren: () =>
          import('@auth/auth.module').then((mod) => mod.AuthModule),
      },
      {
        path: 'cards',
        loadChildren: () =>
          import('@card/card.module').then((mod) => mod.CardModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
