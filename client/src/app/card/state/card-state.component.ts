import { trigger, transition, style, animate } from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { CardService, View, ViewType } from '@card/card.service';

@Component({
  selector: 'app-card-state',
  templateUrl: './card-state.component.html',
  styleUrls: ['./card-state.component.scss'],
  animations: [
    trigger('card', [
      transition(':enter', [
        style({
          width: 0,
        }),
        animate(
          '150ms cubic-bezier(0.0, 0.0, 0.2, 1)',
          style({
            width: '*',
          })
        ),
      ]),
      transition(':leave', [
        animate(
          '100ms cubic-bezier(0.4, 0.0, 1, 1)',
          style({
            width: 0,
          })
        ),
      ]),
    ]),
  ],
})
export class CardStateComponent implements OnInit, OnDestroy {
  private ngOnDestroy$ = new Subject();
  currentView: View;
  viewType: ViewType;
  preview: boolean;

  constructor(private cardService: CardService) {}

  ngOnInit(): void {
    combineLatest([
      this.cardService.currentView,
      this.cardService.viewType,
      this.cardService.preview,
    ])
      .pipe(
        takeUntil(this.ngOnDestroy$),
        map(([currentView, viewType, preview]) => ({
          currentView,
          viewType,
          preview,
        }))
      )
      .subscribe(({ currentView, viewType, preview }) => {
        this.currentView = currentView;
        this.viewType = viewType;
        this.preview = preview;
      });
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }
}
