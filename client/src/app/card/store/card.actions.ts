import { createAction, props } from '@ngrx/store';
import { SnackbarData } from '@shared/components/snackbar/snackbar.component';
import { CardInfo, LoadingType } from '@card/store/card.reducer';

export const requestStart = createAction(
  '[Card] Request Start',
  props<{ loading: { [key in LoadingType]?: boolean } }>()
);

export const requestSuccess = createAction(
  '[Card] Request Success',
  props<{
    loading: { [key in LoadingType]?: boolean };
    snackbar?: SnackbarData;
  }>()
);

export const requestFail = createAction(
  '[Card] Request Fail',
  props<{ error: string; loading: { [key in LoadingType]?: boolean } }>()
);

export const postCard = createAction(
  '[Card] Post Card',
  props<{ card: Omit<CardInfo, 'createdAt' | 'updatedAt' | 'state'> }>()
);

export const fetchCards = createAction('[Card] Fetch Cards');

export const deleteCard = createAction(
  '[Card] Delete Card',
  props<{ cardId: string }>()
);

export const patchCard = createAction(
  '[Card] Patch Card',
  props<{ cardId: string; cardInfo: CardInfo }>()
);
