import mongoose from 'mongoose';
import CONFIGS from './configs';

export const mongooseConnect = () => {
  mongoose.connect(
    CONFIGS.MONGODB_URI,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    },
    () => {
      console.log('Connected to the mongoDB!');
    }
  );
};

export default mongoose;
