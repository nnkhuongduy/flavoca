import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppState } from '@app/store/app.reducer';
import { Store } from '@ngrx/store';
import { combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { selectLoadingState } from '@card/store/card.selectors';
import { CardService } from '@card/card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  animations: [
    trigger('arrowToggle', [
      state(
        'open',
        style({
          width: '25px',
        })
      ),
      state(
        'closed',
        style({
          width: '*',
        })
      ),
      transition('open <=> closed', [
        animate('100ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
      ]),
    ]),
    trigger('itemToggle', [
      state(
        'enter',
        style({
          width: '*',
          visibility: 'initial',
          minWidth: '*',
        })
      ),
      state(
        'leave',
        style({
          width: 0,
          visibility: 'hidden',
          minWidth: 0,
        })
      ),
      transition('leave => enter', [
        animate('200ms cubic-bezier(0.0, 0.0, 0.2, 1)'),
      ]),
      transition('enter => leave', [
        animate('150ms cubic-bezier(0.4, 0.0, 1, 1)'),
      ]),
    ]),
    trigger('containerState', [
      state(
        'active',
        style({
          width: '100vw',
        })
      ),
      state(
        'deactive',
        style({
          width: '*',
        })
      ),
      transition(
        'deactive => active',
        animate('200ms cubic-bezier(0.0, 0.0, 0.2, 1)')
      ),
      transition('active => deactive', [
        animate('150ms cubic-bezier(0.4, 0.0, 1, 1)'),
      ]),
    ]),
  ],
})
export class CardComponent implements OnInit, OnDestroy {
  private ngOnDestroy$ = new Subject();
  toggleStates = false;
  itemStates = true;
  edit: boolean;
  loading: boolean;

  constructor(
    private store: Store<AppState>,
    private cardService: CardService
  ) {}

  ngOnInit(): void {
    combineLatest([
      this.store.select(selectLoadingState, { loadingType: 'global' }),
      this.cardService.edit,
    ])
      .pipe(
        takeUntil(this.ngOnDestroy$),
        map(([loading, edit]) => ({ loading, edit }))
      )
      .subscribe(({ loading, edit }) => {
        this.loading = loading;
        this.edit = edit;
      });
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onMouseEnter() {
    this.toggleStates = true;
  }

  onMouseLeave() {
    this.toggleStates = false;
  }

  onToggle() {
    this.itemStates = !this.itemStates;
  }

  onAddButton() {
    this.cardService.edit.next(!this.edit);
  }
}
