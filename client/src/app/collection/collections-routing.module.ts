import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@app/auth/auth-guard.service';
import { CollectionPageComponent } from './collection-page/collection-page.component';
import { CollectionInteractionComponent } from './collection-page/collection-interaction/collection-interaction.component';
import { CollectionsDashboardComponent } from './collections-dashboard/collections-dashboard.component';
import { CollectionsComponent } from './collections.component';

const routes: Routes = [
  {
    path: '',
    component: CollectionsComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: CollectionsDashboardComponent },
      {
        path: ':collectionName',
        component: CollectionPageComponent,
        children: [{ path: '', component: CollectionInteractionComponent }],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CollectionsRoutingModule {}
