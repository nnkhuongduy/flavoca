import { Action, createReducer, on } from '@ngrx/store';
import {
  requestFail,
  requestStart,
  requestSuccess,
} from '@card/store/card.actions';

export const cardFeatureKey = 'cards';

export enum CardStates {
  new,
  review,
  due,
  suspended,
}

export interface CardInfo {
  _id: string;
  front: string;
  definition: string;
  addition: string[];
  frontInput: boolean;
  collectionId?: string;
  createdAt: string;
  updatedAt: string;
  image?: string;
  audio?: string;
  state: CardStates;
}

export type LoadingType = 'global' | 'post' | 'fetch' | 'collections';

export interface State {
  loadingType: {
    [key in LoadingType]: boolean;
  };
  errorMessage: string;
}

const initialState: State = {
  loadingType: {
    global: false,
    post: false,
    fetch: false,
    collections: false,
  },
  errorMessage: null,
};

const cardReducer = createReducer(
  initialState,
  on(requestStart, (state, { loading }) => {
    return {
      ...state,
      errorMessage: null,
      loadingType: {
        ...state.loadingType,
        ...loading,
      },
    };
  }),
  on(requestSuccess, (state, { loading }) => {
    return {
      ...state,
      loadingType: {
        ...state.loadingType,
        ...loading,
      },
    };
  }),
  on(requestFail, (state, { error, loading }) => {
    return {
      ...state,
      errorMessage: error,
      loadingType: {
        ...state.loadingType,
        ...loading,
      },
    };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return cardReducer(state, action);
}
