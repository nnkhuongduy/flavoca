import { animate, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CardService } from '@card/card.service';
import { UtilitiesService } from '@shared/services/utilities.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-card-additional-input',
  templateUrl: './additional-input.component.html',
  styleUrls: ['./additional-input.component.scss'],
  animations: [
    trigger('state', [
      transition(':enter', [
        style({ width: 0, opacity: 0 }),
        animate(
          '150ms cubic-bezier(0.0, 0.0, 0.2, 1)',
          style({ width: '*', opacity: 1 })
        ),
      ]),
      transition(':leave', [
        animate(
          '100ms cubic-bezier(0.4, 0.0, 1, 1)',
          style({ width: 0, opacity: 0 })
        ),
      ]),
    ]),
  ],
})
export class CardAdditionalInputComponent implements OnInit, OnDestroy {
  @Input() additionalForm: FormControl;
  @Input() index: number;
  @Input() hint: string;
  @Input() placeholder: string;
  private ngOnDestroy$ = new Subject();
  state = false;

  constructor(
    private utilitiesService: UtilitiesService,
    private element: ElementRef,
    private cardService: CardService
  ) {}

  ngOnInit(): void {
    this.utilitiesService.documentClickedTarget
      .pipe(takeUntil(this.ngOnDestroy$))
      .subscribe((target) => this.documentClickListener(target));
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onAInputFocus() {
    this.state = true;
  }

  onDelete() {
    this.cardService.deleteInput(this.index);
  }

  documentClickListener(target: HTMLElement) {
    const elementRef = this.element.nativeElement as HTMLElement;
    if (!elementRef.contains(target)) {
      this.state = false;
    }
  }
}
