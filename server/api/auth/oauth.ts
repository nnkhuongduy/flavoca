import { NextFunction, Router, Request, Response } from 'express';

import Users, { User } from '../../models/user';
import { isAuthenticated } from '../../middlewares/authenticated';
import { getUserClient } from '../../helpers/user';

const oAuthRouter = Router();

interface IUserInformationBasic {
  username: string;
  email: string;
  displayName: string;
  avatar: string;
  password: string | null;
  provider: string;
}

interface IUserInformation extends IUserInformationBasic {
  [key: string]: any;
}

const isValidInformation = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let result: boolean | User;
  let message = 'ERROR_DATABASE';
  try {
    if (!req.body) throw 404;
    const information: IUserInformation = req.body;
    const user = await Users.findById(req.session!.passport.user);
    if (!user) {
      message = 'USER_NOT_FOUND';
      throw 404;
    }
    if (information.password) {
      if (!(await user.comparePassword(information.password))) {
        message = 'INCORRECT_PASSWORD';
        throw 401;
      }
    }
    if (!information.username || !information.email) {
      message = 'INSUFFICIENT_INFORMATION';
      throw 401;
    }
    const userSameEmail = await Users.findOne({
      email: information.email,
      _id: { $ne: user._id },
    });
    if (userSameEmail) {
      message = 'EMAIL_ALREADY_EXISTED';
      throw 401;
    }
    const userSameUsername = await Users.findOne({
      username: information.username,
      _id: { $ne: user._id },
    });
    if (userSameUsername) {
      message = 'USERNAME_ALREADY_EXISTED';
      throw 401;
    }
    next();
  } catch (code) {
    res.statusMessage = message;
    res.sendStatus(!isNaN(code) ? code : 500);
  }
};

oAuthRouter.get('/oauth', isAuthenticated, async (req, res) => {
  const id: string = req.session!.passport.user;
  const user = await Users.findById(id);
  if (!user) {
    res.statusMessage = 'USER_NOT_FOUND';
    return res.sendStatus(404);
  }
  res.json(getUserClient(user));
});

oAuthRouter.post(
  '/update',
  isAuthenticated,
  isValidInformation,
  async (req, res) => {
    const id: string = req.session!.passport.user;
    const {
      username,
      email,
      displayName,
      avatar,
    }: IUserInformationBasic = req.body;

    try {
      const user = await Users.findByIdAndUpdate(
        id,
        {
          username,
          email,
          displayName,
          avatar,
          verified: true,
        },
        { new: true }
      );
      if (user) {
        res.json(getUserClient(user));
      } else {
        res.statusMessage = 'USER_NOT_FOUND';
        res.sendStatus(404);
      }
    } catch (err) {
      res.sendStatus(500);
    }
  }
);

export default oAuthRouter;
