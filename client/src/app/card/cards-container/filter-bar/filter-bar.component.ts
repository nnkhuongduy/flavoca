import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CardCollectionDropdownComponent } from '@card/card-info/collection-dropdown/collection-dropdown.component';
import { DropdownDirective } from '@shared/directives/dropdown.directive';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CardsContainerService } from '../cards-container.service';

@Component({
  selector: 'app-card-filter-bar',
  templateUrl: './filter-bar.component.html',
  styleUrls: ['./filter-bar.component.scss'],
})
export class CardFilterBarComponent implements OnInit, OnDestroy {
  @ViewChild(DropdownDirective) collectionHost: DropdownDirective;
  private collectionDropdownRef: ComponentRef<CardCollectionDropdownComponent>;
  private ngOnDestroy$ = new Subject();
  collectionFilter: string = null;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private cardsContainerService: CardsContainerService
  ) {}

  ngOnInit(): void {
    this.cardsContainerService.collectionFilter
      .pipe(takeUntil(this.ngOnDestroy$))
      .subscribe((collectionName) => {
        this.collectionFilter = collectionName;
      });
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onCollection() {
    const viewContainerRef = this.collectionHost.viewContainerRef;
    viewContainerRef.clear();
    if (!this.collectionDropdownRef) {
      const collectionDropdownResolver = this.componentFactoryResolver.resolveComponentFactory(
        CardCollectionDropdownComponent
      );
      this.collectionDropdownRef = viewContainerRef.createComponent(
        collectionDropdownResolver
      );
      this.collectionDropdownRef.instance.selected.subscribe(
        (collectionName: string) => {
          this.cardsContainerService.collectionFilter.next(collectionName);
          this.cardsContainerService.onFilter();
          this.onCollection();
        }
      );
    } else {
      this.collectionDropdownRef.destroy();
      this.collectionDropdownRef = null;
    }
  }

  onCollectionRemove() {
    this.cardsContainerService.collectionFilter.next(null);
    this.cardsContainerService.onFilter();
  }
}
