import { Action, createReducer, on } from '@ngrx/store';
import {
  collectionRequestSuccess,
  collectionRequestFail,
  collectionRequestStart,
} from '@collection/store/collections.actions';

export const collectionsFeatureKey = 'collections';

export interface CollectionBasic {
  name: string;
  colorCode: string;
}

export interface Collection extends CollectionBasic {
  _id: string;
  createdAt: string;
  updatedAt: string;
  cards: [number, number, number, number];
}

export interface State {
  isLoading: boolean;
  errorMessage: string;
}

const initialState: State = {
  isLoading: false,
  errorMessage: null,
};

const collectionsReducer = createReducer(
  initialState,
  on(collectionRequestStart, (state) => ({
    ...state,
    isLoading: true,
    errorMessage: null,
  })),
  on(collectionRequestSuccess, (state) => ({
    ...state,
    isLoading: false,
    errorMessage: null,
  })),
  on(collectionRequestFail, (state, { error }) => ({
    ...state,
    isLoading: false,
    errorMessage: error,
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return collectionsReducer(state, action);
}
