import { createAction, props } from '@ngrx/store';

import { CollectionBasic } from '@collection/store/collections.reducer';
import { SnackbarData } from '@shared/components/snackbar/snackbar.component';

export const collectionRequestStart = createAction(
  '[Collection] Collection Request Start'
);
export const collectionRequestSuccess = createAction(
  '[Collections] Collection Request Successs',
  props<{ snackbar?: SnackbarData }>()
);
export const collectionRequestFail = createAction(
  '[Collections] Collection Request Fail',
  props<{ error: string }>()
);
export const newCollection = createAction(
  '[Collections] New Collection',
  props<CollectionBasic>()
);
export const deleteCollection = createAction(
  '[Collection] Delete Collection',
  props<{ collectionId: string; keepCards: boolean }>()
);
export const updateCollection = createAction(
  '[Collection] Update Collection',
  props<{ collectionId: string; update: CollectionBasic }>()
);
