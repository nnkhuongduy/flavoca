import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { AppState } from '@app/store/app.reducer';
import { selectIsSigning } from '@auth/store/auth.selectors';
import { signinStart } from '@auth/store/auth.actions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit, OnDestroy {
  private ngDestroy$ = new Subject();
  signinForm: FormGroup;
  signingState: boolean;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.signinForm = new FormGroup({
      identifier: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
    this.store
      .select(selectIsSigning)
      .pipe(takeUntil(this.ngDestroy$))
      .subscribe((state) => (this.signingState = state));
  }

  ngOnDestroy(): void {
    this.ngDestroy$.next();
  }

  onSubmit() {
    this.store.dispatch(signinStart(this.signinForm.value));
  }
}
