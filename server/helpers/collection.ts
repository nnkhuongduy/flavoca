import _ from 'lodash';

export const lowerCase: (collectionName: string) => string = (
  collectionName: string
) => {
  return _.lowerCase(collectionName).replace(/ /g, '');
};
