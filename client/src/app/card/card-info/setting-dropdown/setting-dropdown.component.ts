import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { CardService } from '@card/card.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './setting-dropdown.component.html',
  styleUrls: ['./setting-dropdown.component.scss'],
})
export class CardSettingDropdownComponent implements OnInit, OnDestroy {
  private ngOnDestroy$ = new Subject();
  frontInput: boolean;

  constructor(private cardService: CardService) {}

  ngOnInit(): void {
    this.frontInput = this.cardService.cardForm.get('frontInput').value;
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onCheckbox(event: MatCheckboxChange) {
    this.cardService.patchForm({ frontInput: event.checked });
  }
}
