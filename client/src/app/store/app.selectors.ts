import { createSelector } from '@ngrx/store';
import { AppState } from './app.reducer';

export const selectApp = (state: AppState) => state.app;

export const selectUser = createSelector(selectApp, (state) => state.user);

export const selectCollections = createSelector(
  selectApp,
  (state) => state.collections
);

export const selectCollection = createSelector(selectApp, (state, props) =>
  state.collections.find((collection) => collection._id === props.collectionId)
);

export const selectCards = createSelector(selectApp, (state) => state.cards);

export const selectCardsDic = createSelector(
  selectApp,
  (state) => state.cardsDic
);

export const selectCard = createSelector(selectApp, (state, props) =>
  state.cards.find((card) => card._id === props.cardId)
);

export const selectCardInDic = createSelector(
  selectApp,
  (state, props: { cardId: string }) => state.cardsDic[props.cardId]
);
