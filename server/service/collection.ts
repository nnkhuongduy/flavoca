import { Request, Response } from 'express';

import mongoose from '../configs/mongoose';
import Users from '../models/user';
import { Collection } from '../models/collection';
import Cards from '../models/card';
import { errorHandler } from '../helpers/global';

const minifyCollections = (collection: Collection[]) => {
  return collection.map<
    Pick<Collection, 'name' | 'colorCode' | 'createdAt' | 'updatedAt'>
  >((collectionEle) => ({
    _id: collectionEle._id,
    name: collectionEle.name,
    colorCode: collectionEle.colorCode,
    createdAt: collectionEle.createdAt,
    updatedAt: collectionEle.updatedAt,
  }));
};

export const createCollection = async (req: Request, res: Response) => {
  // Create mongoose session
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    if (req.session) {
      // Get user and collection from request
      const user = await Users.findById(req.session.passport.user).session(
        session
      );
      const collection: Pick<Collection, 'name' | 'colorCode'> = req.body;

      // User and collection error handler
      if (!user) {
        throw Error('USER_NOT_FOUND');
      }
      if (user.validateCollection(collection.name)) {
        throw Error('COLLECTION_ALREADY_EXISTED');
      }

      // Push new collection into collections
      user.collections.push(collection as Collection);

      // Save user information
      await user.save();

      // Commit the mongoose session
      await session.commitTransaction();

      // Respone
      res.json(minifyCollections(user.collections));
    } else {
      throw Error('USER_NOT_FOUND');
    }
  } catch (error) {
    await session.abortTransaction();
    errorHandler(res, error.message);
  }
};

export const getCollections = async (req: Request, res: Response) => {
  try {
    if (req.session) {
      const user = await Users.findById(req.session.passport.user);

      if (!user) {
        throw Error('USER_NOT_FOUND');
      }

      const counts = await Promise.all(
        user.collections.map(async (collectionEle) => {
          const count = await collectionEle.countCards();
          return {
            _id: collectionEle._id,
            name: collectionEle.name,
            colorCode: collectionEle.colorCode,
            createdAt: collectionEle.createdAt,
            updatedAt: collectionEle.updatedAt,
            cards: count,
          };
        })
      );

      res.json(counts);

      // res.json(minifyCollections(user.collections));
    } else {
      throw Error('USER_NOT_FOUND');
    }
  } catch (error) {
    errorHandler(res, error.message);
  }
};

export const deleteCollection = async (req: Request, res: Response) => {
  // Create mongoose session
  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    if (req.session) {
      // Destructuring the request body
      const collectionId: string = req.body.collectionId;
      const keepCards: boolean = req.body.keepCards;

      // Find the user
      const user = await Users.findById(req.session.passport.user).session(
        session
      );

      // No user found handler
      if (!user) {
        throw Error('USER_NOT_FOUND');
      }

      // Validate the collection
      const collectionToBeDeleted = user.collections.find(
        (collectionEle) => collectionEle._id.toString() === collectionId
      );

      // Collection invalidation handler
      if (!collectionToBeDeleted) {
        throw Error('COLLECTION_NOT_FOUND');
      }

      // Delete all the cards if checked
      if (!keepCards) {
        Cards.deleteMany({ _collectionId: collectionToBeDeleted._id }).session(
          session
        );
      }

      // Delete the collection
      user.collections = user.collections.filter(
        (collection) => collection.name !== collectionToBeDeleted.name
      );

      // Save the user's data
      await user.save();

      // Commit mongoose session
      await session.commitTransaction();

      // Respone
      res.json(minifyCollections(user.collections));
    } else {
      throw new Error('USER_NOT_FOUND');
    }
  } catch (error) {
    await session.abortTransaction();
    errorHandler(res, error.message);
  }
};

export const updateCollection = async (req: Request, res: Response) => {
  // Create mongoose session
  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    if (req.session) {
      // Deconstructing request body
      const collectionId: string = req.body.collectionId;
      const {
        name,
        colorCode,
      }: { name: string; colorCode: string } = req.body.update;

      // Get user
      const user = await Users.findById(req.session.passport.user).session(
        session
      );

      // No user error handler
      if (!user) {
        throw Error('USER_NOT_FOUND');
      }

      // Find target collection for update
      const collectionTarget = user.collections.find(
        (collection) => collection._id.toString() === collectionId
      );

      // No target collection error handler
      if (!collectionTarget) {
        throw Error('COLLECTION_NOT_FOUND');
      }

      // Update target collection
      collectionTarget.name = name;
      collectionTarget.colorCode = colorCode;

      // Save user information
      await user.save();

      // Commit mongoose session
      await session.commitTransaction();

      // Respone
      res.json(minifyCollections(user.collections));
    } else {
      throw Error('USER_NOT_FOUND');
    }
  } catch (error) {
    await session.abortTransaction();
    errorHandler(res, error.message);
  }
};
