import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AuthGuard } from '@auth/auth-guard.service';
import { SharedModule } from '@shared/shared.module';
import { CardRoutingModule } from '@card/card-routing.module';
import { CardComponent } from '@card/card.component';
import { cardFeatureKey, reducer } from '@card/store/card.reducer';
import { CardsContainerComponent } from '@card/cards-container/cards-container.component';
import { CardCollectionDialogComponent } from '@card/cards-container/collection-dialog/collection-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardInfoComponent } from '@card/card-info/card-info.component';
import { CardAdditionalInputComponent } from '@card/card-info/additional-input/additional-input.component';
import { CardSettingDropdownComponent } from '@card/card-info/setting-dropdown/setting-dropdown.component';
import { CardEditComponent } from '@card/state/edit/card-edit.component';
import { CardActiveComponent } from '@card/state/active/card-active.component';
import { CardCollectionDropdownComponent } from '@card/card-info/collection-dropdown/collection-dropdown.component';
import { CardEffect } from '@card/store/card.effects';
import { CardStateComponent } from '@card/state/card-state.component';
import { CardService } from '@card/card.service';
import { CardConfirmationComponent } from '@card/confirmation/confirmation.component';
import { CardFilterBarComponent } from '@card/cards-container/filter-bar/filter-bar.component';
import { CardsContainerService } from '@card/cards-container/cards-container.service';

@NgModule({
  declarations: [
    CardComponent,
    CardsContainerComponent,
    CardCollectionDialogComponent,
    CardInfoComponent,
    CardAdditionalInputComponent,
    CardSettingDropdownComponent,
    CardEditComponent,
    CardActiveComponent,
    CardCollectionDropdownComponent,
    CardStateComponent,
    CardConfirmationComponent,
    CardFilterBarComponent,
  ],
  imports: [
    CardRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature(cardFeatureKey, reducer),
    EffectsModule.forFeature([CardEffect]),
  ],
  providers: [AuthGuard],
})
export class CardModule {}
