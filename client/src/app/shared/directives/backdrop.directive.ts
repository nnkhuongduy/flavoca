import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appBackdrop]',
})
export class BackdropDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
