import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { lowerCase } from 'lodash';
import { take } from 'rxjs/operators';

import { AppState } from '@app/store/app.reducer';
import { selectCollection, selectCollections } from '@app/store/app.selectors';
import { CardStates } from '@card/store/card.reducer';
import { Collection } from '@collection/store/collections.reducer';

@Injectable({
  providedIn: 'root',
})
export class CollectionsService {
  constructor(private store: Store<AppState>) {}

  convertCollectionName(collectionName: string) {
    return lowerCase(collectionName).replace(/ /g, '');
  }

  async onSearch(query: string) {
    const collections = await this.store
      .select(selectCollections)
      .pipe(take(1))
      .toPromise();

    return collections.filter((collection) =>
      this.convertCollectionName(collection.name).includes(query)
    );
  }

  getCountCards(
    collection: Collection,
    type: keyof typeof CardStates | 'total'
  ) {
    if (type !== 'total') {
      return collection.cards[CardStates[type]];
    } else {
      return collection.cards.reduce((total, card) => total + card, 0);
    }
  }
}
