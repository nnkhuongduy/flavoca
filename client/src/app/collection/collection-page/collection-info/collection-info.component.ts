import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { CardService } from '@card/card.service';
import { CardsContainerService } from '@card/cards-container/cards-container.service';

import { Collection } from '@collection/store/collections.reducer';
import { DropdownDirective } from '@shared/directives/dropdown.directive';
import { CollectionSettingDropdownComponent } from './setting-dropdow/setting-dropdow.component';

@Component({
  selector: 'app-collection-info',
  templateUrl: './collection-info.component.html',
  styleUrls: ['./collection-info.component.scss'],
})
export class CollectionInfoComponent {
  @Input() collection: Collection;
  @ViewChild(DropdownDirective) settingHost: DropdownDirective;
  private dropdownState = false;
  private dropdownRef: ComponentRef<CollectionSettingDropdownComponent>;
  isLoading = false;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private router: Router,
    private cardService: CardService,
    private cardsContainerService: CardsContainerService
  ) {}

  onSetting() {
    const settingHostView = this.settingHost.viewContainerRef;
    settingHostView.clear();

    if (!this.dropdownState) {
      const settingDropdownComponent = this.componentFactoryResolver.resolveComponentFactory(
        CollectionSettingDropdownComponent
      );
      this.dropdownRef = this.settingHost.viewContainerRef.createComponent(
        settingDropdownComponent
      );
    } else {
      this.dropdownRef.destroy();
    }

    this.dropdownState = !this.dropdownState;
  }

  onAddCard() {
    this.router.navigate(['cards']);
    this.cardService.removeSelected();
    this.cardService.edit.next(true);
    this.cardService.patchForm({ collectionId: this.collection._id });
    this.cardsContainerService.collectionFilter.next(this.collection.name);
    this.cardsContainerService.filterState.next(true);
    this.cardsContainerService.onFilter();
  }

  onCardsList() {
    this.router.navigate(['cards']);
    this.cardService.edit.next(false);
    this.cardsContainerService.collectionFilter.next(this.collection.name);
    this.cardsContainerService.filterState.next(true);
    this.cardsContainerService.onFilter();
  }
}
