import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@app/shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthComponent } from '@auth/auth.component';
import * as fromAuth from '@auth/store/auth.reducer';
import { AuthEffect } from '@auth/store/auth.effects';
import { UpdateInfoComponent } from './update-info/update-info.component';
import { ConfirmationDialogComponent } from './update-info/confirmation-dialog/confirmation-dialog.component';
import { AuthGuard } from './auth-guard.service';

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    AuthComponent,
    UpdateInfoComponent,
    ConfirmationDialogComponent,
  ],
  imports: [
    AuthRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    HttpClientModule,
    StoreModule.forFeature(fromAuth.authFeatureKey, fromAuth.reducer),
    EffectsModule.forFeature([AuthEffect]),
  ],
  providers: [AuthGuard],
})
export class AuthModule {}
