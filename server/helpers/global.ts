import { Response } from 'express';

export const errorHandler = (
  res: Response,
  errorMessage?: string,
  errorCode?: number
): void => {
  if (errorMessage) {
    res.statusMessage = errorMessage;
  }
  if (errorCode) {
    res.sendStatus(errorCode);
  } else {
    res.sendStatus(500);
  }
};
