import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { lowerCase } from 'lodash';

import { Collection } from '@collection/store/collections.reducer';
import { AppState } from '@app/store/app.reducer';
import { takeUntil } from 'rxjs/operators';
import { selectCollections } from '@app/store/app.selectors';
import { CardStates } from '@card/store/card.reducer';

@Component({
  selector: 'app-collection-interaction',
  templateUrl: './collection-interaction.component.html',
  styleUrls: ['./collection-interaction.component.scss'],
})
export class CollectionInteractionComponent implements OnInit, OnDestroy {
  private ngOnDestroy$ = new Subject();
  collection: Collection;

  constructor(private route: ActivatedRoute, private store: Store<AppState>) {}

  ngOnInit(): void {
    let collectionName: string;
    this.route.params.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
      collectionName = params.collectionName;
    });
    this.store
      .select(selectCollections)
      .pipe(takeUntil(this.ngOnDestroy$))
      .subscribe((collections) => {
        if (collections) {
          this.collection = collections.find(
            (collection) =>
              lowerCase(collection.name).replace(/ /g, '') === collectionName
          );
        }
      });
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  getCard(type: keyof typeof CardStates) {
    return this.collection.cards[CardStates[type]];
  }
}
