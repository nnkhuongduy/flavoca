import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '@app/store/app.reducer';
import { signoutStart } from '@app/store/app.actions';

@Component({
  templateUrl: './user-dropdown.component.html',
  styleUrls: ['./user-dropdown.component.scss'],
})
export class UserDropdownComponent {
  constructor(private store: Store<AppState>) {}

  onSignout() {
    this.store.dispatch(signoutStart());
  }
}
