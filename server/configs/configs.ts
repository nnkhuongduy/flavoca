import path from 'path';
require('dotenv').config({ path: path.join(__dirname, '../../.env') });
const mongoAdmin = process.env.MONGO_ADMIN;
const mongoPassword = process.env.MONGO_PASSWORD;
const mongoDB = process.env.MONGO_DB;
export default {
  PORT: 8000,
  // MONGODB_URI: 'mongodb://localhost:27017/flavocaDB',
  MONGODB_URI: `mongodb+srv://${mongoAdmin}:${mongoPassword}@cluster0.alnrp.mongodb.net/${mongoDB}?retryWrites=true&w=majority`,
};
