import { createAction, props } from '@ngrx/store';

import { User, AppState } from '@app/store/app.reducer';
import { CardInfo } from '@card/store/card.reducer';
import { Collection } from '@collection/store/collections.reducer';

export const autoLogin = createAction('[App] Auto Login Start');

export const autoLoginFail = createAction('[App] Auto Login Failed');

export const setUser = createAction('[App] Set User', props<User | null>());

export const signoutStart = createAction('[App] Sign Out Start');

export const signoutSuccess = createAction('[App] Sign Out Success');

export const signoutFail = createAction('[App] Sign Out Fail');

export const setCollections = createAction(
  '[App] Set Collections',
  props<{ collections: Collection[] | null }>()
);

export const setCards = createAction(
  '[App] Set Cards',
  props<{
    cards: Pick<
      CardInfo,
      '_id' | 'front' | 'collectionId' | 'updatedAt' | 'createdAt'
    >[];
  }>()
);

export const fetchCollections = createAction(
  '[App] Fetch Collections',
  props<{ loading?: keyof AppState }>()
);

export const fetchCard = createAction(
  '[App] Fetch Single Card',
  props<{ cardId: string }>()
);

export const setCardDic = createAction(
  '[App] Set Card Into Dic',
  props<{
    cardId: string;
    cardInfo: CardInfo;
  }>()
);

export const setCard = createAction(
  '[App] Set Card',
  props<{
    cardId: string;
    cardInfo: Pick<
      CardInfo,
      '_id' | 'collectionId' | 'createdAt' | 'updatedAt' | 'front'
    >;
  }>()
);
