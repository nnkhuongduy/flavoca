import { AppState } from '@app/store/app.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import {
  State as CardState,
  cardFeatureKey,
  LoadingType,
} from '@card/store/card.reducer';

const selectState = createFeatureSelector<AppState, CardState>(cardFeatureKey);

export const selectLoadingState = createSelector(
  selectState,
  (state, props) => state.loadingType[props.loadingType]
);

export const selectError = createSelector(
  selectState,
  (state) => state.errorMessage
);
