import {
  Component,
  ComponentFactoryResolver,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AppState, User } from '@app/store/app.reducer';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';

import { takeUntil } from 'rxjs/operators';
import { selectUser } from '@app/store/app.selectors';
import { DropdownDirective } from '@app/shared/directives/dropdown.directive';
import { UserDropdownComponent } from './user-dropdown/user-dropdown.component';
import { UtilitiesService } from '@app/shared/services/utilities.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @ViewChild(DropdownDirective) dropdownHost: DropdownDirective;
  @ViewChild('userContainer') userContainer: ElementRef;
  private ngOnDestroy$ = new Subject();
  private isDropdown = false;
  user: User;
  userAvatarValid = true;

  constructor(
    private store: Store<AppState>,
    private componentFactoryResolver: ComponentFactoryResolver,
    private utilitiesService: UtilitiesService
  ) {}

  ngOnInit(): void {
    this.store
      .select(selectUser)
      .pipe(takeUntil(this.ngOnDestroy$))
      .subscribe((user) => {
        this.user = user;
      });
    this.utilitiesService.documentClickedTarget
      .pipe(takeUntil(this.ngOnDestroy$))
      .subscribe((target) => this.documentClickListener(target));
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onAvatarError() {
    this.userAvatarValid = false;
  }

  onDropdown() {
    const viewContainerRef = this.dropdownHost.viewContainerRef;
    viewContainerRef.clear();
    if (!this.isDropdown) {
      const userDropdown = this.componentFactoryResolver.resolveComponentFactory(
        UserDropdownComponent
      );
      viewContainerRef.createComponent(userDropdown);
    }
    this.isDropdown = !this.isDropdown;
  }

  documentClickListener(target: HTMLElement) {
    if (this.isDropdown && this.userContainer) {
      if (!this.userContainer.nativeElement.contains(target)) {
        this.onDropdown();
      }
    }
  }
}
