import { Request, Response, NextFunction } from 'express';

export const isAuthenticated = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.session && req.session.passport && req.session.passport.user) {
    return next();
  }
  res.statusMessage = 'ERROR_AUTHENTICATE';
  res.sendStatus(401);
};
