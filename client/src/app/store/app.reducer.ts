import { Action, createReducer, on } from '@ngrx/store';

import * as fromCollections from '@collection/store/collections.reducer';
import * as fromAuth from '@auth/store/auth.reducer';
import * as fromCards from '@card/store/card.reducer';
import {
  setCard,
  setCardDic,
  setCards,
  setCollections,
  setUser,
  signoutSuccess,
} from '@app/store/app.actions';
import { Collection } from '@collection/store/collections.reducer';

export interface AppState {
  app: State;
  collections: fromCollections.State;
  auth: fromAuth.State;
  cards: fromCards.State;
}

export interface User {
  username: string;
  email: string;
  avatar: string;
  displayName: string;
  provider: string;
  verified: boolean;
  password?: string;
  createdAt: string;
  updatedAt: string;
}

interface State {
  user: User | null;
  collections: Collection[] | null;
  cards: Pick<
    fromCards.CardInfo,
    '_id' | 'front' | 'collectionId' | 'createdAt' | 'updatedAt'
  >[];
  cardsDic: {
    [key: string]: fromCards.CardInfo;
  };
}

const initialState: State = {
  user: null,
  collections: [],
  cards: null,
  cardsDic: {},
};

const appReducer = createReducer(
  initialState,
  on(setUser, (state, user) => ({
    ...state,
    user: user
      ? {
          ...state.user,
          ...user,
          type: undefined,
        }
      : null,
  })),
  on(signoutSuccess, (state) => ({
    ...state,
    user: null,
  })),
  on(setCollections, (state, { collections }) => ({
    ...state,
    collections: [...collections],
  })),
  on(setCards, (state, { cards }) => ({
    ...state,
    cards,
  })),
  on(setCardDic, (state, { cardId, cardInfo }) => ({
    ...state,
    cardsDic: {
      ...state.cardsDic,
      [cardId]: cardInfo,
    },
  })),
  on(setCard, (state, { cardId, cardInfo }) => {
    let cardState = [...state.cards];
    const index = cardState.findIndex((card) => card._id === cardId);
    if (index !== -1) {
      if (cardInfo) {
        cardState[index] = cardInfo;
      } else {
        cardState = cardState.filter((card) => card._id !== cardId);
      }
    } else {
      cardState.push(cardInfo);
    }
    return {
      ...state,
      cards: cardState,
    };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return appReducer(state, action);
}
