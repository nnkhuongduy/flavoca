import { createAction, props } from '@ngrx/store';

import { User } from '@app/store/app.reducer';

interface UserBasicInfo {
  indentifier: string | null;
  password: string;
}

export interface UserInfo extends UserBasicInfo {
  email: string;
  username: string;
}

export const signupStart = createAction(
  '[Auth] Signup Start',
  props<UserInfo>()
);

export const signinStart = createAction(
  '[Auth] Signin Start',
  props<UserBasicInfo>()
);

export const setErrorMessage = createAction(
  '[Auth] Set Error Message',
  props<{ message: string | null }>()
);

export const oAuthStart = createAction('[Auth] OAuth Start');

export const updateStart = createAction('[Auth] Update Start', props<User>());

export const authRequestFail = createAction(
  '[Auth] Request Fail',
  props<{ error: string }>()
);

export const authRequestSuccess = createAction('[Auth] Request Success');
