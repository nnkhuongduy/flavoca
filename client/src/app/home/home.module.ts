import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { HomeGuard } from './home-guard.service';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [SharedModule, HomeRoutingModule],
  providers: [HomeGuard],
})
export class HomeModule {}
