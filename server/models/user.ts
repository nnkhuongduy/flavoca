import { Schema, model, Document, Model } from 'mongoose';
import bcrypt from 'bcrypt';
import validator from 'validator';

import { collectionSchema, Collection } from './collection';
import { lowerCase } from '../helpers/collection';
import Cards from './card';

type TOAuth = 'google' | 'facebook' | 'local';

export interface User extends Document {
  username: string;
  email: string;
  password: string;
  googleID: string;
  facebookID: string;
  displayName: string;
  avatar: string;
  provider: TOAuth;
  createdAt: string;
  updatedAt: string;
  verified: boolean;
  collections: Collection[];
  providerID: string;
  comparePassword: (candidatePassword: string) => Promise<boolean>;
  validateCollection: (collectionName: string) => boolean;
  isHavingCollection: (collectionId: string) => boolean;
  isOAuth: () => boolean;
}

interface UserModel extends Model<User> {
  validateOAuthId: (oauth: TOAuth, id: string) => Promise<boolean>;
}

type UserClientProps =
  | 'username'
  | 'email'
  | 'displayName'
  | 'avatar'
  | 'provider'
  | 'createdAt'
  | 'updatedAt'
  | 'verified';

export type UserClient = Pick<User, UserClientProps>;

const userSchema = new Schema(
  {
    username: {
      type: String,
      lowercase: true,
      trim: true,
      unique: true,
    },
    email: {
      type: String,
      lowercase: true,
      unique: true,
      validate: (str: string) => validator.isEmail(str),
    },
    password: String,
    googleID: String,
    facebookID: String,
    displayName: String,
    avatar: String,
    provider: {
      type: String,
      enum: ['facebook', 'google', 'local'],
      required: true,
    },
    verified: {
      type: Boolean,
      default: false,
    },
    collections: [collectionSchema],
  },
  { timestamps: { createdAt: true, updatedAt: true } }
);

userSchema.virtual('providerID').get(function (this: User) {
  return this.googleID ? this.googleID : this.facebookID;
});

userSchema.methods.isOAuth = function (this: User) {
  return this.provider !== 'local';
};

userSchema.methods.comparePassword = async function (
  this: User,
  candidatePassword: string
): Promise<boolean> {
  return bcrypt.compare(candidatePassword, this.password);
};

userSchema.methods.validateCollection = function (
  this: User,
  collectionName: string
) {
  return !!this.collections.find(
    (collection) => collection.name === collectionName
  );
};

userSchema.methods.isHavingCollection = function (
  this: User,
  collectionId: string
) {
  return !!this.collections.find(
    (collection) => collection._id.toString() === collectionId
  );
};

userSchema.statics.validateOAuthId = function (
  oauth: TOAuth,
  id: string
): Promise<boolean> {
  return new Promise(async (resolve, reject) => {
    try {
      if (oauth !== 'facebook' && oauth !== 'google') {
        resolve(false);
      } else {
        const user = await this.findOne({ [`${oauth}Id`]: id });
        if (user) {
          resolve(false);
        } else {
          resolve(true);
        }
      }
    } catch (error) {
      reject(error);
    }
  });
};

userSchema.pre<User>('save', async function (next) {
  if (this.password) {
    if (!this.isModified('password')) {
      return next();
    }
    const hashedPassword = await bcrypt.hash(this.password, 10);
    this.password = hashedPassword;
    return next();
  }
});

userSchema.post<User>('save', async (doc) => {
  if (doc.isNew) {
    await new Cards({
      userId: doc._id,
      cards: [],
    }).save();
  }
});

export default model<User, UserModel>('Users', userSchema, 'Users');
