import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { faGoogle, faFacebook } from '@fortawesome/free-brands-svg-icons';
import * as fromApp from '@app/store/app.reducer';
import {
  selectIsSigning,
  selectErrorMessage,
} from '@auth/store/auth.selectors';
import { WindowRef } from '@shared/services/window.service';
import { setErrorMessage } from '@auth/store/auth.actions';

type OAuth = 'google' | 'facebook';

const LOCAL_ERROR_CODE = '_ec';
const LOCAL_AUTHENTICATE = '_au';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit, OnDestroy {
  ngDestroyed$ = new Subject();
  signingState: boolean;
  errorMessage: string;
  faGoogle = faGoogle;
  faFacebook = faFacebook;

  constructor(
    private store: Store<fromApp.AppState>,
    private winRef: WindowRef,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.store
      .select(selectIsSigning)
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe((signingState) => {
        this.signingState = signingState;
      });
    this.store
      .select(selectErrorMessage)
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe((message) => {
        this.errorMessage = message;
      });
    this.route.queryParams
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe((params: { authenticated?: boolean; errorCode?: string }) => {
        if (this.winRef.nativeWindow) {
          if (params.errorCode) {
            localStorage.setItem(LOCAL_ERROR_CODE, params.errorCode);
          }
          if (params.authenticated) {
            localStorage.setItem(LOCAL_AUTHENTICATE, 'true');
          }
          this.winRef.nativeWindow.close();
        }
      });
  }

  ngOnDestroy(): void {
    this.winRef.nativeWindow.removeEventListener(
      'storage',
      this.onStorage.bind(this)
    );

    this.ngDestroyed$.next();

    this.store.dispatch(setErrorMessage({ message: null }));
  }

  onStorage() {
    const errorCode = localStorage.getItem(LOCAL_ERROR_CODE);
    const authenticate = localStorage.getItem(LOCAL_AUTHENTICATE);
    if (errorCode) {
      this.errorMessage = errorCode;
      localStorage.removeItem(LOCAL_ERROR_CODE);
    }
    if (authenticate) {
      this.router.navigate(['auth', 'oauth']);
      localStorage.removeItem(LOCAL_AUTHENTICATE);
    }
  }

  onSigninOAuth(type: OAuth) {
    window.open(`/api/auth/${type}`, '_blank', 'top=50%');
    this.winRef.nativeWindow.addEventListener(
      'storage',
      this.onStorage.bind(this)
    );
  }

  errorConfirmation() {
    this.store.dispatch(setErrorMessage({ message: null }));
  }
}
