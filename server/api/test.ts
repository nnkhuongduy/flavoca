import { Router } from 'express';

const checkRouter = Router();

checkRouter.get('/check', (req, res) => {
  res.send();
});

export default checkRouter;
