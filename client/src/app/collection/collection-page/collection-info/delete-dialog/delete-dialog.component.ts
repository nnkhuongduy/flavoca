import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppState } from '@app/store/app.reducer';
import { deleteCollection } from '@collection/store/collections.actions';
import { Collection } from '@collection/store/collections.reducer';
import { Store } from '@ngrx/store';

@Component({
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
})
export class CollectionDeleteDialogComponent {
  check = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public collection: Collection,
    private dialogRef: MatDialogRef<CollectionDeleteDialogComponent>,
    private store: Store<AppState>
  ) {}

  onDelete() {
    this.store.dispatch(
      deleteCollection({
        collectionId: this.collection._id,
        keepCards: this.check,
      })
    );
    this.dialogRef.close(true);
  }

  get getTotalCard() {
    return this.collection.cards.reduce((total, card) => total + card, 0);
  }
}
