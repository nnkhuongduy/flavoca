import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { map, takeUntil } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { combineLatest, Subject } from 'rxjs';

import { selectCollections } from '@app/store/app.selectors';
import { AppState } from '@app/store/app.reducer';
import { fetchCollections } from '@app/store/app.actions';
import { CardService } from '@card/card.service';
import { selectLoadingState } from '@card/store/card.selectors';
import { Collection } from '@collection/store/collections.reducer';

@Component({
  templateUrl: './collection-dropdown.component.html',
  styleUrls: ['./collection-dropdown.component.scss'],
})
export class CardCollectionDropdownComponent implements OnInit, OnDestroy {
  private ngOnDestroy$ = new Subject();
  selected = new EventEmitter<string>();
  collections: Collection[];
  loading: boolean;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    combineLatest([
      this.store.select(selectCollections),
      this.store.select(selectLoadingState, { loadingType: 'collections' }),
    ]).pipe(takeUntil(this.ngOnDestroy$));
    this.store
      .select(selectCollections)
      .pipe(takeUntil(this.ngOnDestroy$))
      .subscribe((collections) => {
        if (!collections) {
          this.store.dispatch(fetchCollections({ loading: 'cards' }));
        } else {
          this.collections = collections;
        }
      });
    this.store
      .select(selectLoadingState, { loadingType: 'collections' })
      .pipe(takeUntil(this.ngOnDestroy$))
      .subscribe((loadingState) => (this.loading = loadingState));
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onSelect(collectionId: string) {
    this.selected.emit(collectionId);
  }
}
