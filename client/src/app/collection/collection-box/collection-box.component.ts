import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AppState } from '@app/store/app.reducer';
import { Store } from '@ngrx/store';
import { map, takeUntil } from 'rxjs/operators';
import { combineLatest, Subject } from 'rxjs';

import { selectCollection } from '@app/store/app.selectors';
import { Collection } from '@collection/store/collections.reducer';
import { selectIsLoading } from '@collection/store/collections.selectors';
import { CardStates } from '@card/store/card.reducer';
import { CollectionsService } from '@collection/collections.service';

@Component({
  selector: 'app-collection-box',
  templateUrl: './collection-box.component.html',
  styleUrls: ['./collection-box.component.scss'],
})
export class CollectionBoxComponent implements OnInit, OnDestroy {
  private ngOnDestroy$ = new Subject();
  @Input() collectionId: string;
  collection: Collection;
  disabled = false;

  constructor(
    private store: Store<AppState>,
    private collectionsService: CollectionsService
  ) {}

  ngOnInit() {
    combineLatest([
      this.store.select(selectIsLoading),
      this.store.select<Collection, { collectionId: string }>(
        selectCollection,
        { collectionId: this.collectionId }
      ),
    ])
      .pipe(
        takeUntil(this.ngOnDestroy$),
        map(([loading, collection]) => ({ loading, collection }))
      )
      .subscribe(({ loading, collection }) => {
        this.disabled = loading;
        this.collection = collection;
      });
  }

  ngOnDestroy() {
    this.ngOnDestroy$.next();
  }

  getCard(type: keyof typeof CardStates | 'total') {
    return this.collectionsService.getCountCards(this.collection, type);
  }
}
