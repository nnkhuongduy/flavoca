import { Router } from 'express';
import { User } from 'models/user';
import passport from '../../configs/passport';

const facebookAuthRouter = Router();

facebookAuthRouter.get('/facebook', passport.authenticate('facebook'));

facebookAuthRouter.get('/facebook/callback', (req, res) => {
  passport.authenticate(
    'facebook',
    (
      error: Error,
      user: User | false,
      info: { message: string } | undefined
    ) => {
      if (error) {
        return res.redirect(`/auth/?errorCode=${info!.message}`);
      }
      if (user === false) {
        return res.redirect(`/auth/?errorCode=${info!.message}`);
      }
      req.login(user, (err) => {
        if (err) return res.redirect('/auth/?errorCode=ERROR_AUTHENTICATE');
        res.redirect('/auth/?authenticated=true');
      });
    }
  )(req, res);
});

export default facebookAuthRouter;
