import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import * as fromApp from '@app/store/app.reducer';
import { newCollection } from '@collection/store/collections.actions';
import { CollectionDialogComponent } from '@collection/collection-dialog/collection-dialog.component';

@Component({
  selector: 'app-collection-create',
  templateUrl: './collection-create.component.html',
  styleUrls: ['./collection-create.component.scss'],
})
export class CollectionCreateComponent implements OnInit {
  constructor(
    private dialog: MatDialog,
    private store: Store<fromApp.AppState>
  ) {}

  ngOnInit(): void {}

  onCreateCollection() {
    const dialogRef = this.dialog.open(CollectionDialogComponent, {
      width: '60vw',
      maxHeight: '80%',
    });

    dialogRef.componentInstance.mode = 'CREATE';

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.store.dispatch(
          newCollection({
            name: result.collectionName,
            colorCode: result.colorCode,
          })
        );
      }
    });
  }
}
