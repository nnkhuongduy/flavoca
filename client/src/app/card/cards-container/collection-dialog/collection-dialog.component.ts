import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

import { AppState } from '@app/store/app.reducer';
import { Collection } from '@collection/store/collections.reducer';

@Component({
  templateUrl: './collection-dialog.component.html',
})
export class CardCollectionDialogComponent implements OnInit, OnDestroy {
  private ngOnDestroy$ = new Subject();
  collections: Collection[];
  collectionForm: FormControl;
  index: number;

  constructor(
    private store: Store<AppState>,
    private dialog: MatDialogRef<CardCollectionDialogComponent>
  ) {}

  ngOnInit(): void {
    // this.store
    //   .select(selectSelectedCollections)
    //   .pipe(takeUntil(this.ngOnDestroy$))
    //   .subscribe((collections) => (this.collections = collections));
    // this.collectionForm = new FormControl('', Validators.required);
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onConfirm() {
    this.dialog.close(this.index);
  }
}
