import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { AppState } from '@app/store/app.reducer';
import { fetchCollections } from '@app/store/app.actions';
import { selectIsLoading } from '@collection/store/collections.selectors';
import { selectCollections } from '@app/store/app.selectors';
import { Collection } from '@collection/store/collections.reducer';
import { CollectionsService } from '@collection/collections.service';

@Component({
  selector: 'app-collections-dashboard',
  templateUrl: './collections-dashboard.component.html',
  styleUrls: ['./collections-dashboard.component.scss'],
})
export class CollectionsDashboardComponent implements OnInit, OnDestroy {
  private ngDestroy$ = new Subject();
  collections: Collection[];
  isLoading: boolean;

  constructor(
    private store: Store<AppState>,
    private collectionsService: CollectionsService
  ) {}

  ngOnInit(): void {
    combineLatest([
      this.store.select(selectIsLoading),
      this.store.select(selectCollections),
    ])
      .pipe(
        takeUntil(this.ngDestroy$),
        map(([loading, collections]) => ({ loading, collections }))
      )
      .subscribe(({ loading, collections }) => {
        this.isLoading = loading;
        this.collections = collections;
      });
  }

  ngOnDestroy(): void {
    this.ngDestroy$.next();
  }

  onRefresh() {
    this.store.dispatch(fetchCollections({ loading: 'collections' }));
  }

  async onSearch(event: KeyboardEvent) {
    this.collections = await this.collectionsService.onSearch(
      (event.target as HTMLInputElement).value
    );
  }

  getCollectionLink(collectionName: string) {
    return this.collectionsService.convertCollectionName(collectionName);
  }
}
