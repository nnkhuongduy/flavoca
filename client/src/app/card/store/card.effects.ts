import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {
  catchError,
  exhaustMap,
  map,
  mergeMap,
  take,
  tap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';

import {
  postCard,
  requestSuccess,
  requestFail,
  requestStart,
  fetchCards,
  deleteCard,
  patchCard,
} from '@card/store/card.actions';
import { CardInfo } from '@card/store/card.reducer';
import { AppState } from '@app/store/app.reducer';
import {
  fetchCollections,
  setCards,
  setCard,
  setCardDic,
} from '@app/store/app.actions';
import { CardService } from '@card/card.service';
import { SnackbarComponent } from '@shared/components/snackbar/snackbar.component';

@Injectable()
export class CardEffect {
  constructor(
    private store: Store<AppState>,
    private action$: Actions,
    private http: HttpClient,
    private cardService: CardService,
    private snackbar: MatSnackBar
  ) {}

  postCard$ = createEffect(() =>
    this.action$.pipe(
      ofType(postCard),
      tap(() => {
        this.store.dispatch(
          requestStart({ loading: { post: true, global: true } })
        );
      }),
      exhaustMap((action) =>
        this.http.post<CardInfo>('/api/cards', action.card).pipe(
          mergeMap((card) => {
            this.cardService.setSelectedCard(card._id);
            return [
              setCard({
                cardId: card._id,
                cardInfo: {
                  _id: card._id,
                  front: card.front,
                  collectionId: card.collectionId,
                  createdAt: card.createdAt,
                  updatedAt: card.updatedAt,
                },
              }),
              setCardDic({
                cardId: card._id,
                cardInfo: card,
              }),
              requestSuccess({
                loading: { post: false, global: false },
                snackbar: {
                  message: `Successfully creating ${action.card.front} card`,
                  severity: 'success',
                },
              }),
            ];
          }),
          catchError((error: HttpErrorResponse) =>
            of(
              requestFail({
                error: error.statusText,
                loading: { post: false, global: false },
              })
            )
          )
        )
      )
    )
  );

  fetchCards$ = createEffect(() =>
    this.action$.pipe(
      ofType(fetchCards),
      tap(() =>
        this.store.dispatch(
          requestStart({ loading: { fetch: true, global: true } })
        )
      ),
      exhaustMap(() =>
        this.http
          .get<
            Pick<
              CardInfo,
              '_id' | 'front' | 'collectionId' | 'createdAt' | 'updatedAt'
            >[]
          >('/api/cards')
          .pipe(
            mergeMap((cards) => [
              setCards({ cards }),
              requestSuccess({ loading: { fetch: false, global: false } }),
            ]),
            catchError((error: HttpErrorResponse) =>
              of(
                requestFail({
                  error: error.statusText,
                  loading: { fetch: false, global: false },
                })
              )
            )
          )
      )
    )
  );

  deleteCard$ = createEffect(() =>
    this.action$.pipe(
      ofType(deleteCard),
      tap(() =>
        this.store.dispatch(
          requestStart({
            loading: {
              global: true,
              post: true,
            },
          })
        )
      ),
      exhaustMap((action) =>
        this.http
          .put('api/cards', {
            cardId: action.cardId,
            type: 'DELETE',
          })
          .pipe(
            tap(() =>
              this.store.dispatch(fetchCollections({ loading: 'cards' }))
            ),
            mergeMap(() => {
              this.cardService.removeSelected();
              return [
                requestSuccess({
                  loading: {
                    global: false,
                    post: false,
                  },
                  snackbar: {
                    message: `Successfully deleting the card`,
                    severity: 'success',
                  },
                }),
                setCard({ cardId: action.cardId, cardInfo: undefined }),
                setCardDic({ cardId: action.cardId, cardInfo: undefined }),
              ];
            }),
            catchError((error: HttpErrorResponse) =>
              of(
                requestFail({
                  error: error.statusText,
                  loading: {
                    global: false,
                    post: false,
                  },
                })
              )
            )
          )
      )
    )
  );

  patchCard$ = createEffect(() =>
    this.action$.pipe(
      ofType(patchCard),
      tap(() => {
        this.store.dispatch(
          requestStart({
            loading: {
              global: true,
              post: true,
            },
          })
        );
      }),
      exhaustMap(({ cardId, cardInfo }) =>
        this.http
          .patch<CardInfo>('api/cards', { cardId, cardInfo })
          .pipe(
            mergeMap((card) => [
              setCard({
                cardId: card._id,
                cardInfo: {
                  _id: card._id,
                  front: card.front,
                  collectionId: card.collectionId,
                  createdAt: card.createdAt,
                  updatedAt: card.updatedAt,
                },
              }),
              setCardDic({
                cardId: card._id,
                cardInfo: card,
              }),
              requestSuccess({
                loading: {
                  global: false,
                  post: false,
                },
                snackbar: {
                  message: `Successfully editing ${cardInfo.front} card`,
                  severity: 'success',
                },
              }),
            ]),
            catchError((error: HttpErrorResponse) =>
              of(
                requestFail({
                  error: error.statusText,
                  loading: {
                    global: false,
                    post: false,
                  },
                })
              )
            )
          )
      )
    )
  );

  requestSuccess$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(requestSuccess),
        tap(({ snackbar }) => {
          if (snackbar) {
            this.snackbar.openFromComponent(SnackbarComponent, {
              data: snackbar,
              panelClass: ['snackbar', `snackbar-${snackbar.severity}`],
            });
          }
        })
      ),
    { dispatch: false }
  );

  requestFail$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(requestFail),
        tap((error) => {
          this.cardService.showError({
            message: error.error,
            severity: 'error',
          });
        })
      ),
    { dispatch: false }
  );
}
