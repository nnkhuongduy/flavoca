import { Schema, model, Document, Types, Model } from 'mongoose';

export enum CardStates {
  new,
  review,
  due,
  suspended,
}

export interface CardBase {
  front: string;
  definition: string;
  addition: string[];
  frontInput: boolean;
  createdAt: string;
  updatedAt: string;
  image?: string;
  audio?: string;
  state: number;
  collectionId: string;
}

export interface Card extends Document, CardBase {}

// export type CardStates = 'new' | 'review' | 'due' | 'suspended';

// export const CARD_STATES = ['new', 'review', 'due', 'suspended'] as const;

export const cardsSchema = new Schema(
  {
    front: {
      type: String,
      required: true,
    },
    definition: {
      type: String,
      required: true,
    },
    addition: [String],
    frontInput: {
      type: Boolean,
      default: false,
    },
    image: String,
    audio: String,
    state: {
      type: Number,
      required: true,
      min: 0,
      max: 4,
      default: 0,
    },
    collectionId: {
      type: Types.ObjectId,
      ref: 'Collections',
      required: true,
    },
  },
  { timestamps: { createdAt: true, updatedAt: true } }
);

export default model<Card, Model<Card>>('Cards', cardsSchema, 'Cards');
