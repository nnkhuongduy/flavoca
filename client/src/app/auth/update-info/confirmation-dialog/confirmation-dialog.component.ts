import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmationDialogComponent implements OnInit {
  confirmForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<ConfirmationDialogComponent>) {}

  ngOnInit(): void {
    this.confirmForm = new FormGroup({
      password: new FormControl('', Validators.required),
    });
  }

  onSubmit() {
    if (this.confirmForm.valid) {
      this.dialogRef.close(this.confirmForm.value.password);
    }
  }
}
