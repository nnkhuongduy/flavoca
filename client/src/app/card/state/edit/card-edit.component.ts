import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { combineLatest, Subject } from 'rxjs';
import { trigger, transition, style, animate } from '@angular/animations';
import { map, takeUntil } from 'rxjs/operators';

import { ViewType, View } from '@card/card.service';
import { CardSettingDropdownComponent } from '@card/card-info/setting-dropdown/setting-dropdown.component';
import { DropdownDirective } from '@shared/directives/dropdown.directive';
import { CardService, Media } from '@card/card.service';
import { UtilitiesService } from '@shared/services/utilities.service';

@Component({
  selector: 'app-card-edit',
  templateUrl: './card-edit.component.html',
  styleUrls: ['./card-edit.component.scss'],
  animations: [
    trigger('input', [
      transition(':enter', [
        style({
          opacity: 0,
        }),
        animate(
          '350ms cubic-bezier(0.0, 0.0, 0.2, 1)',
          style({
            opacity: 1,
          })
        ),
      ]),
      transition(':leave', [
        animate('300ms cubic-bezier(0.4, 0.0, 1, 1)', style({ opacity: 0 })),
      ]),
    ]),
  ],
})
export class CardEditComponent implements OnInit, OnDestroy {
  @Input() state: View;
  @ViewChild(DropdownDirective) settingHost: DropdownDirective;
  @ViewChild('dropdown') dropdownRef: ElementRef;
  private settingDropdownRef: ComponentRef<CardSettingDropdownComponent>;
  private ngOnDestroy$ = new Subject();
  cardForm: FormGroup;
  viewType: ViewType;
  currentView: View;
  media = {
    image: false,
    audio: false,
  };

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private cardService: CardService,
    private utilitiesService: UtilitiesService
  ) {}

  ngOnInit(): void {
    this.cardForm = this.cardService.cardForm;
    combineLatest([this.cardService.viewType, this.cardService.currentView])
      .pipe(
        takeUntil(this.ngOnDestroy$),
        map(([viewType, currentView]) => ({
          viewType,
          currentView,
        }))
      )
      .subscribe(({ viewType, currentView }) => {
        this.viewType = viewType;
        this.currentView = currentView;
      });
    this.utilitiesService.documentClickedTarget
      .pipe(takeUntil(this.ngOnDestroy$))
      .subscribe((target) => this.documentClickListener(target));
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onAddInput() {
    this.cardService.addInput();
  }

  onMedia(mediaType: Media) {
    this.media[mediaType] = !this.media[mediaType];
  }

  onDeleteInput(index: number) {
    this.cardService.deleteInput(index);
  }

  onSetting() {
    const viewContainerRef = this.settingHost.viewContainerRef;
    if (!this.settingDropdownRef) {
      const settingDropdown = this.componentFactoryResolver.resolveComponentFactory(
        CardSettingDropdownComponent
      );
      this.settingDropdownRef = viewContainerRef.createComponent(
        settingDropdown
      );
    } else {
      viewContainerRef.clear();
      this.settingDropdownRef = null;
    }
  }

  documentClickListener(target: HTMLElement) {
    if (this.settingDropdownRef && this.dropdownRef) {
      if (!this.dropdownRef.nativeElement.contains(target)) {
        this.onSetting();
      }
    }
  }

  get additionalControl() {
    return (this.cardForm.get('addition') as FormArray).controls;
  }

  get imageControl() {
    return this.cardForm.get('image');
  }
}
