import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { map, switchMap, take } from 'rxjs/operators';

import { autoLogin, setUser } from '@app/store/app.actions';
import { AppState } from '@app/store/app.reducer';
import { selectUser } from '@app/store/app.selectors';

@Injectable()
export class HomeGuard implements CanActivate {
  constructor(
    private store: Store<AppState>,
    private router: Router,
    private action$: Actions
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Promise<boolean | UrlTree>
    | Observable<boolean | UrlTree> {
    return this.store.select(selectUser).pipe(
      take(1),
      switchMap((user) => {
        if (user) {
          return of(this.router.createUrlTree(['collections']));
        }
        this.store.dispatch(autoLogin());
        return this.action$.pipe(
          ofType(setUser),
          take(1),
          map((loggedUser) => {
            if (loggedUser.username) {
              return this.router.createUrlTree(['collections']);
            }
            return true;
          })
        );
      })
    );
  }
}
