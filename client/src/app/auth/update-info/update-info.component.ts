import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AppState } from '@app/store/app.reducer';
import {
  selectErrorMessage,
  selectIsSigning,
} from '@auth/store/auth.selectors';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { updateStart } from '../store/auth.actions';
import { selectUser } from '@app/store/app.selectors';

@Component({
  selector: 'app-update-info',
  templateUrl: './update-info.component.html',
  styleUrls: ['./update-info.component.scss'],
})
export class UpdateInfoComponent implements OnInit, OnDestroy {
  private userProvider: string;
  private userPassword: string;
  private ngDestroy$ = new Subject();
  updateForm: FormGroup;
  errorCode: string;

  constructor(private store: Store<AppState>, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.updateForm = new FormGroup({
      username: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      displayName: new FormControl(''),
      avatar: new FormControl(''),
    });
    this.store
      .select(selectUser)
      .pipe(takeUntil(this.ngDestroy$))
      .subscribe((user) => {
        if (user) {
          this.updateForm.setValue({
            username: user.username ? user.username : '',
            email: user.email ? user.email : '',
            displayName: user.displayName ? user.displayName : '',
            avatar: user.avatar ? user.avatar : '',
          });
          this.userProvider = user.provider;
        }
      });
    this.store
      .select(selectErrorMessage)
      .pipe(takeUntil(this.ngDestroy$))
      .subscribe((error) => {
        this.errorCode = error;
      });
    this.store
      .select(selectIsSigning)
      .pipe(takeUntil(this.ngDestroy$))
      .subscribe((state) => {
        if (state) {
          this.updateForm.disable();
        } else {
          this.updateForm.enable();
        }
      });
  }

  ngOnDestroy(): void {
    this.ngDestroy$.next();
  }

  onUpdate() {
    if (this.updateForm.valid) {
      if (this.userProvider === 'local') {
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
          minWidth: '300px',
          width: '40%',
        });
        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            this.userPassword = result;
            this.dispatchUpdate();
          }
        });
      } else {
        this.dispatchUpdate();
      }
    }
  }

  dispatchUpdate() {
    this.store.dispatch(
      updateStart({
        ...this.updateForm.value,
        password: this.userPassword,
        provider: this.userProvider,
      })
    );
  }
}
