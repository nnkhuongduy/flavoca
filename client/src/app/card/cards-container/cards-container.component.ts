import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

import { AppState } from '@app/store/app.reducer';
import { selectCards, selectCollections } from '@app/store/app.selectors';
import { fetchCards } from '@card/store/card.actions';
import { CardInfo } from '@card/store/card.reducer';
import { selectLoadingState } from '@card/store/card.selectors';
import { CardService } from '@card/card.service';
import { animate, style, transition, trigger } from '@angular/animations';
import { CardsContainerService } from './cards-container.service';
import { fetchCollections } from '@app/store/app.actions';
import { Collection } from '@collection/store/collections.reducer';

const columnsToDisplay = ['front', 'collection', 'updatedAt', 'createdAt'];

@Component({
  selector: 'app-cards-container',
  templateUrl: './cards-container.component.html',
  styleUrls: ['./cards-container.component.scss'],
  animations: [
    trigger('search', [
      transition(':enter', [
        style({ opacity: 0, height: 1 }),
        animate(
          '250ms cubic-bezier(0.0, 0.0, 0.2, 1)',
          style({ opacity: 1, height: 'auto' })
        ),
      ]),
      transition(':leave', [
        animate(
          '200ms cubic-bezier(0.4, 0.0, 1, 1)',
          style({ opacity: 0, height: 0 })
        ),
      ]),
    ]),
  ],
})
export class CardsContainerComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @Output() addButtonClick = new EventEmitter<void>();
  @Input() addButtonState: boolean;
  @ViewChild(MatSort) sort: MatSort;
  private ngOnDestroy$ = new Subject();
  loading: boolean;
  displayedColumns: string[];
  searchValue: string;
  searchState = false;
  filterState = true;
  collections: Collection[];

  constructor(
    private store: Store<AppState>,
    private cardService: CardService,
    private cardsContainerService: CardsContainerService
  ) {}

  ngOnInit(): void {
    combineLatest([
      this.store.select(selectCards),
      this.store.select(selectCollections),
      this.store.select(selectLoadingState, { loadingType: 'fetch' }),
      this.cardService.edit,
      this.cardsContainerService.searchState,
      this.cardsContainerService.filterState,
    ])
      .pipe(
        takeUntil(this.ngOnDestroy$),
        map(([cards, collections, loading, edit, search, filter]) => ({
          cards,
          collections,
          loading,
          edit,
          search,
          filter,
        }))
      )
      .subscribe(({ cards, collections, loading, edit, search, filter }) => {
        this.loading = loading;
        this.searchState = search;
        this.filterState = filter;
        if (edit) {
          this.displayedColumns = columnsToDisplay.slice(0, 3);
        } else {
          this.displayedColumns = columnsToDisplay.slice();
        }
        if (!cards) {
          this.store.dispatch(fetchCards());
        } else {
          this.cardsContainerService.dataSource.data = cards;
          this.cardsContainerService.dataSource._updateChangeSubscription();
        }
        if (loading) {
          this.cardsContainerService.dataSource.data = [];
          this.cardsContainerService.dataSource._updateChangeSubscription();
        }
        if (!collections) {
          this.store.dispatch(fetchCollections({ loading: 'cards' }));
        } else {
          this.collections = collections;
        }
      });
  }

  ngAfterViewInit(): void {
    this.cardsContainerService.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onAdd() {
    this.addButtonClick.emit();
  }

  onRefresh() {
    this.store.dispatch(fetchCards());
  }

  async onRowClick(row: CardInfo) {
    if (!this.cardService.edit.value) {
      this.cardService.edit.next(true);
    }
    this.cardService.setSelectedCard(row._id);
  }

  isSelected(row: CardInfo): boolean {
    const selectedId = this.cardService.selectedId.value;
    return selectedId && selectedId === row._id;
  }

  onSearch() {
    this.cardsContainerService.nameFilter.next(this.searchValue);
    this.cardsContainerService.onFilter();
  }

  onSearchClear() {
    this.searchValue = '';
    this.onSearch();
  }

  onSearchState() {
    this.cardsContainerService.searchState.next(!this.searchState);
  }

  onFilterState() {
    this.cardsContainerService.filterState.next(!this.filterState);
  }

  get dataSource() {
    return this.cardsContainerService.dataSource;
  }

  getCollectionName(collectionId: string) {
    if (this.collections) {
      const collection = this.collections.find(
        (collectionEle) => collectionEle._id === collectionId
      );
      return collection ? collection.name : '';
    }
    return '';
  }
}
