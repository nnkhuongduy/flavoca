import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'error' })
export class ErrorPipe implements PipeTransform {
  transform(value: string): string {
    switch (value) {
      case 'USER_ALREADY_EXISTED':
        return 'This user is already existed!';
      case 'ERROR_DATABASE':
        return 'The database is currently down! Please try again later.';
      case 'ERROR_AUTHENTICATE':
        return 'There was a error in authentication process! Please try again later.';
      case 'INCORRECT_PASSWORD':
        return 'The password is incorrect!';
      case 'USER_NOT_FOUND':
        return 'This user cannot be found!';
      case 'EMAIL_ALREADY_EXISTED':
        return 'This email is already being used!';
      case 'USERNAME_ALREADY_EXISTED':
        return 'This username is already being used!';
      case 'INSUFFICIENT_INFORMATION':
        return 'Your information is insufficient!';
      case 'COLLECTION_ALREADY_EXISTED':
        return 'This collection already existed!';
      case 'CARD_ALREADY_EXISTED':
        return 'There is already a card with this word!';
      default:
        return 'There was a error in the process! Please try again.';
    }
  }
}
