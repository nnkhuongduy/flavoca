import { Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BehaviorSubject, Subject } from 'rxjs';

import { convertCollectionName } from '@helper/collection';
import { CardInfo } from '@card/store/card.reducer';

@Injectable({
  providedIn: 'root',
})
export class CardsContainerService {
  collectionFilter = new BehaviorSubject<string>(null);
  nameFilter = new BehaviorSubject<string>('');
  dataSource = new MatTableDataSource<
    Pick<CardInfo, '_id' | 'front' | 'collectionId' | 'createdAt' | 'updatedAt'>
  >();
  searchState = new BehaviorSubject<boolean>(false);
  filterState = new BehaviorSubject<boolean>(false);

  constructor() {
    // this.dataSource.filterPredicate = (data: CardInfo, filter: string) => {
    //   const collectionFilter = this.collectionFilter.value;
    //   const nameFilter = this.nameFilter.value;
    //   const isName = data.front
    //     .trim()
    //     .replace(/ /g, '')
    //     .toLowerCase()
    //     .includes(nameFilter.trim().replace(/ /g, '').toLowerCase());
    //   const isCollection =
    //     collectionFilter !== null
    //       ? convertCollectionName(data.collection) ===
    //         convertCollectionName(collectionFilter)
    //       : true;
    //   return isName && isCollection;
    // };
  }

  onFilter() {
    this.dataSource.filter = 'test';
  }
}
