import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromApp from '@app/store/app.reducer';
import * as fromAuth from '@auth/store/auth.actions';
import { selectIsSigning } from '@auth/store/auth.selectors';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  signupForm: FormGroup;
  signingState: boolean;

  constructor(private store: Store<fromApp.AppState>) {}

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      username: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });

    this.store.select(selectIsSigning).subscribe((state) => {
      this.signingState = state;
      if (this.signingState) {
        this.signupForm.disable();
      } else {
        this.signupForm.enable();
      }
    });
  }

  onSubmit() {
    this.store.dispatch(fromAuth.signupStart(this.signupForm.value));
  }
}
