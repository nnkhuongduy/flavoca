import { Router } from 'express';
import { getUserClient } from '../../helpers/user';

import passport from '../../configs/passport';
import Users, { User } from '../../models/user';

const localAuthRouter = Router();

localAuthRouter.post('/local/signup', async (req, res) => {
  try {
    const existed = await Users.findOne({
      $or: [{ username: req.body.username }, { email: req.body.email }],
    });
    if (existed) {
      res.statusMessage = 'USER_ALREADY_EXISTED';
      res.sendStatus(401);
    } else {
      const newUser = await new Users({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        provider: 'local',
        verified: true,
      }).save();
      req.login(newUser, (err) => {
        if (err) {
          res.statusMessage = 'ERROR_AUTHENTICATE';
          return res.sendStatus(500);
        }
        res.json(getUserClient(newUser));
      });
    }
  } catch (err) {
    res.statusMessage = 'ERROR_DATABASE';
    res.sendStatus(500);
  }
});

localAuthRouter.post('/local/signin', (req, res) => {
  passport.authenticate(
    'local',
    (error: Error, user: User | false, info: { message: string }) => {
      if (error) {
        res.statusMessage = 'ERROR_DATABASE';
        return res.sendStatus(500);
      }
      if (user === false) {
        res.statusMessage = info.message;
        return res.sendStatus(401);
      }
      req.login(user, (err) => {
        if (err) {
          res.statusMessage = 'ERROR_AUTHENTICATE';
          return res.sendStatus(500);
        }
        res.json(getUserClient(user));
      });
    }
  )(req, res);
});

export default localAuthRouter;
