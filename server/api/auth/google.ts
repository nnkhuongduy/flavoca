import { Router } from 'express';
import { User } from 'models/user';
import passport from '../../configs/passport';

const googleAuthRouter = Router();

googleAuthRouter.get(
  '/google',
  passport.authenticate('google', {
    scope: ['email', 'profile', 'openid'],
  })
);

googleAuthRouter.get('/google/callback', (req, res) => {
  passport.authenticate(
    'google',
    (err: Error, user: User | false, info: { message: string } | undefined) => {
      if (err) {
        if (info) {
          return res.redirect(`/auth/?errorCode=${info.message}`);
        }
        return res.redirect('/auth/?errorCode=ERROR_DATABASE');
      }
      if (user === false && info) {
        return res.redirect(`/auth/?errorCode=${info.message}`);
      }
      req.login(user, (error) => {
        if (error) {
          return res.redirect('/auth/?errorCode=ERROR_AUTHENTICATE');
        }
        res.redirect('/auth/?authenticated=true');
      });
    }
  )(req, res);
});

export default googleAuthRouter;
