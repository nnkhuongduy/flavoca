import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { BackdropComponent } from './components/backdrop/backdrop.component';
import { SnackbarComponent } from './components/snackbar/snackbar.component';
import { DirectiveModule } from './directives/directive.module';
import { ErrorPipe } from './pipes/error.pipe';
import { MaterialModule } from './material.module';

@NgModule({
  declarations: [BackdropComponent, SnackbarComponent, ErrorPipe],
  imports: [CommonModule, MaterialModule, DirectiveModule],
  exports: [
    CommonModule,
    MaterialModule,
    BackdropComponent,
    SnackbarComponent,
    ErrorPipe,
    DirectiveModule,
  ],
  providers: [
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 3500 } },
  ],
})
export class SharedModule {}
