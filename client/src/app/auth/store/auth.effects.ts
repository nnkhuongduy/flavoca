import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';

import {
  signupStart,
  authRequestFail,
  authRequestSuccess,
  signinStart,
  oAuthStart,
  updateStart,
} from '@auth/store/auth.actions';
import { User } from '@app/store/app.reducer';
import { setUser } from '@app/store/app.actions';

@Injectable()
export class AuthEffect {
  signup$ = createEffect(() =>
    this.action$.pipe(
      ofType(signupStart),
      exhaustMap((action) => {
        return this.http.post<User>('/api/auth/local/signup', action).pipe(
          mergeMap((user) => {
            this.router.navigate(['collections']);
            return [setUser(user), authRequestSuccess()];
          }),
          catchError((error: HttpErrorResponse) => {
            return of(authRequestFail({ error: error.statusText }));
          })
        );
      })
    )
  );

  signin$ = createEffect(() =>
    this.action$.pipe(
      ofType(signinStart),
      exhaustMap((action) =>
        this.http.post<User>('/api/auth/local/signin', action).pipe(
          mergeMap((user) => [setUser(user), authRequestSuccess()]),
          catchError((error: HttpErrorResponse) =>
            of(
              authRequestFail({
                error: error.statusText,
              })
            )
          )
        )
      )
    )
  );

  oauth$ = createEffect(() =>
    this.action$.pipe(
      ofType(oAuthStart),
      exhaustMap(() =>
        this.http.get<User>('/api/auth/oauth').pipe(
          mergeMap((user) => {
            if (user.verified) {
              this.router.navigate(['collections']);
            } else {
              this.router.navigate(['auth', 'update']);
            }
            return [setUser(user), authRequestSuccess()];
          }),
          catchError((error: HttpErrorResponse) =>
            of(
              authRequestFail({
                error: error.statusText,
              })
            )
          )
        )
      )
    )
  );

  update$ = createEffect(() =>
    this.action$.pipe(
      ofType(updateStart),
      exhaustMap((action) =>
        this.http.post<User>('/api/auth/update', action).pipe(
          mergeMap((user) => {
            this.router.navigate(['collections']);
            return [setUser(user), authRequestSuccess()];
          }),
          catchError((error: HttpErrorResponse) =>
            of(
              authRequestFail({
                error: error.statusText,
              })
            )
          )
        )
      )
    )
  );

  constructor(
    private action$: Actions,
    private http: HttpClient,
    private router: Router
  ) {}
}
