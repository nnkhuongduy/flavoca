import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appDropdown]',
})
export class DropdownDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
