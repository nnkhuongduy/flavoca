import { createFeatureSelector, createSelector } from '@ngrx/store';

import { AppState } from '@app/store/app.reducer';
import {
  State as CollectionState,
  collectionsFeatureKey,
} from '@collection/store/collections.reducer';

export const selectState = createFeatureSelector<AppState, CollectionState>(
  collectionsFeatureKey
);

export const selectIsLoading = createSelector(
  selectState,
  (state) => state.isLoading
);

export const selectError = createSelector(
  selectState,
  (state) => state.errorMessage
);
