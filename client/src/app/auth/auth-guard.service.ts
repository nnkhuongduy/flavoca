import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';

import { AppState } from '@app/store/app.reducer';
import { selectUser } from '@app/store/app.selectors';
import { Injectable } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { autoLogin, setUser } from '@app/store/app.actions';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private store: Store<AppState>,
    private router: Router,
    private action$: Actions
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Promise<boolean | UrlTree>
    | Observable<boolean | UrlTree> {
    return this.store.select(selectUser).pipe(
      take(1),
      switchMap((user) => {
        if (user) {
          return of(true);
        }
        this.store.dispatch(autoLogin());
        return this.action$.pipe(
          ofType(setUser),
          take(1),
          map((loggedUser) => {
            if (loggedUser) {
              return true;
            }
            return this.router.createUrlTree(['/']);
          })
        );
      })
    );
  }
}
