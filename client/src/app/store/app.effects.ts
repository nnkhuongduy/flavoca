import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap, mergeMap } from 'rxjs/operators';

import { AppState, User } from '@app/store/app.reducer';
import {
  autoLogin,
  autoLoginFail,
  setUser,
  signoutFail,
  signoutStart,
  signoutSuccess,
  fetchCollections,
  setCollections,
  fetchCard,
  setCardDic,
} from '@app/store/app.actions';
import { Store } from '@ngrx/store';
import {
  collectionRequestFail,
  collectionRequestStart,
  collectionRequestSuccess,
} from '@collection/store/collections.actions';
import {
  requestStart as cardRequestStart,
  requestSuccess as cardRequestSuccess,
  requestFail as cardRequestFail,
} from '@card/store/card.actions';
import { Collection } from '@collection/store/collections.reducer';
import { CardInfo } from '@card/store/card.reducer';

const loadingHandle = (
  action: { loading?: keyof AppState; type: string },
  callbacks: Partial<{ [key in keyof AppState]: () => void }>
) => {
  if (action.loading) {
    callbacks[action.loading]();
  }
};

@Injectable()
export class AppEffects {
  constructor(
    private http: HttpClient,
    private action$: Actions,
    private router: Router,
    private store: Store<AppState>
  ) {}

  autoLogin$ = createEffect(() =>
    this.action$.pipe(
      ofType(autoLogin),
      exhaustMap(() =>
        this.http.get<User>('/api/auth/signin').pipe(
          map((user) => {
            return setUser(user);
          }),
          catchError(() => {
            return of(autoLoginFail(), setUser(null));
          })
        )
      )
    )
  );

  signout$ = createEffect(() =>
    this.action$.pipe(
      ofType(signoutStart),
      exhaustMap(() =>
        this.http.get('/api/auth/signout', { responseType: 'text' }).pipe(
          map(() => {
            this.router.navigate(['/']);
            return signoutSuccess();
          }),
          catchError((error) => {
            return of(signoutFail());
          })
        )
      )
    )
  );

  fetchCollections$ = createEffect(() => {
    let loadingState = true;
    return this.action$.pipe(
      ofType(fetchCollections),
      tap((action) => {
        loadingHandle(action, {
          collections: () => this.store.dispatch(collectionRequestStart()),
          cards: () =>
            this.store.dispatch(
              cardRequestStart({ loading: { collections: loadingState } })
            ),
        });
      }),
      exhaustMap((action) =>
        this.http.get<Collection[]>('/api/collections').pipe(
          tap(() => (loadingState = false)),
          map((collections) => {
            loadingHandle(action, {
              collections: () =>
                this.store.dispatch(collectionRequestSuccess({})),
              cards: () =>
                this.store.dispatch(
                  cardRequestSuccess({ loading: { collections: loadingState } })
                ),
            });
            return setCollections({ collections });
          }),
          catchError((error: HttpErrorResponse) => {
            loadingHandle(action, {
              collections: () =>
                this.store.dispatch(
                  collectionRequestFail({ error: error.statusText })
                ),
              cards: () =>
                this.store.dispatch(
                  cardRequestFail({
                    error: error.statusText,
                    loading: { collections: loadingState },
                  })
                ),
            });
            return of(setCollections(null));
          })
        )
      )
    );
  });

  fetchCard = createEffect(() =>
    this.action$.pipe(
      ofType(fetchCard),
      tap(() =>
        this.store.dispatch(
          cardRequestStart({ loading: { global: true, post: true } })
        )
      ),
      exhaustMap((action) =>
        this.http.get<CardInfo>(`/api/cards/${action.cardId}`).pipe(
          mergeMap((card) => [
            setCardDic({ cardId: action.cardId, cardInfo: card }),
            cardRequestSuccess({ loading: { global: false, post: false } }),
          ]),
          catchError((error: HttpErrorResponse) => {
            return of(
              cardRequestFail({
                error: error.statusText,
                loading: { global: false, post: false },
              })
            );
          })
        )
      )
    )
  );
}
