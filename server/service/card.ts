import { Request, Response, NextFunction } from 'express';

import Users from '../models/user';
import mongoose from '../configs/mongoose';
import { errorHandler } from '../helpers/global';
import Cards, { CardBase } from '../models/card';

export const postCard = async (req: Request, res: Response) => {
  // Start mongoose session
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    if (req.session) {
      // Destructuring from the request body
      const cardInfo: Omit<CardBase, 'createdAt' | 'updatedAt' | 'state'> =
        req.body;

      // Find user from passport session
      const user = await Users.findById(req.session.passport.user).session(
        session
      );

      // No user found handler
      if (!user) {
        throw Error('USER_NOT_FOUND');
      }

      // Check if collection exist
      if (!user.isHavingCollection(cardInfo.collectionId)) {
        throw Error('COLLECTION_NOT_FOUND');
      }

      // Create and save new Card
      const newCard = await new Cards({
        front: cardInfo.front,
        definition: cardInfo.definition,
        addition: cardInfo.addition,
        frontInput: cardInfo.frontInput,
        image: cardInfo.image,
        audio: cardInfo.audio,
        collectionId: cardInfo.collectionId,
      }).save();

      // Commit mongoose session
      await session.commitTransaction();

      res.json(newCard);
    } else {
      throw Error('ERROR_AUTHENTICATE');
    }
  } catch (error) {
    await session.abortTransaction();
    errorHandler(res, error.message);
  }
};

export const getCards = async (req: Request, res: Response) => {
  try {
    if (req.session) {
      // Get user
      const user = await Users.findById(req.session.passport.user);

      // No user handler
      if (!user) {
        throw Error('USER_NOT_FOUND');
      }

      interface CardToBeSent {
        _id: string;
        collectionId: string;
        front: string;
        createdAt: string;
        updatedAt: string;
      }

      // Create empty cards array
      let cards: CardToBeSent[] = [];

      // Find cards belong to collection and add it to cards array
      for (const collection of user.collections) {
        const cardsWithId = await Cards.find({ collectionId: collection._id });
        cards = [
          ...cards,
          ...cardsWithId.map<CardToBeSent>((card) => ({
            _id: card._id,
            front: card.front,
            createdAt: card.createdAt,
            updatedAt: card.updatedAt,
            collectionId: card.collectionId,
          })),
        ];
      }

      // Respone
      res.json(cards);
    } else {
      throw Error('ERROR_AUTHENTICATE');
    }
  } catch (error) {
    errorHandler(res, error.message);
  }
};

export const cardActions = async (req: Request, res: Response) => {
  if (req.body.type === 'DELETE') {
    await deleteCard(req, res);
  }
};

export const deleteCard = async (req: Request, res: Response) => {
  // Create mongoose session
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    if (req.session) {
      // Find user from passport session
      const user = await Users.findById(req.session.passport.user).session(
        session
      );

      // No user found handler
      if (!user) {
        throw Error('USER_NOT_FOUND');
      }

      // Destructuring request body
      const id = req.body.cardId;

      const deletedCard = await Cards.findByIdAndDelete(id).session(session);

      // Commit mongoose session
      await session.commitTransaction();

      res.json(deletedCard);
    } else {
      throw Error('ERROR_AUTHENTICATE');
    }
  } catch (error) {
    await session.abortTransaction();
    errorHandler(res, error.message);
  }
};

export const patchCard = async (req: Request, res: Response) => {
  // Starting mongoose session
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    if (req.session) {
      // Find user from passport session
      const user = await Users.findById(req.session.passport.user).session(
        session
      );

      // No user found handler
      if (!user) {
        throw Error('USER_NOT_FOUND');
      }

      // Deconstructing information from reques body
      const _id: string = req.body.cardId;
      const editInfo = req.body.cardInfo;

      // Find and edit the card
      const editedCard = await Cards.findByIdAndUpdate(_id, editInfo, {
        new: true,
      }).session(session);

      // Commit mongoose session
      await session.commitTransaction();

      res.json(editedCard);
    } else {
      throw Error('ERROR_AUTHENTICATE');
    }
  } catch (error) {
    await session.abortTransaction();
    errorHandler(res, error.message);
  }
};

export const getCard = async (req: Request, res: Response) => {
  try {
    if (req.session) {
      const cardId = req.params.cardId;
      const card = await Cards.findById(cardId);

      if (!card) {
        throw Error('DATABASE_ERROR');
      }

      res.json({
        _id: card._id,
        front: card.front,
        definition: card.definition,
        addition: card.addition,
        frontInput: card.frontInput,
        collectionId: card.collectionId,
        createdAt: card.createdAt,
        updatedAt: card.updatedAt,
        image: card.image,
        audio: card.audio,
        state: card.state,
      });
    } else {
      throw Error('USER_NOT_FOUND');
    }
  } catch (error) {
    errorHandler(res, error.message);
  }
};
