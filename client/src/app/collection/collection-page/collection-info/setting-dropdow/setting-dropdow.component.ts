import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { CollectionDialogComponent } from '@collection/collection-dialog/collection-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '@app/store/app.reducer';
import { selectCollections } from '@app/store/app.selectors';
import { map, takeUntil } from 'rxjs/operators';
import { Collection } from '@collection/store/collections.reducer';
import { CollectionDeleteDialogComponent } from '@collection/collection-page/collection-info/delete-dialog/delete-dialog.component';

export type Mode = 'CHANGE' | 'DELETE';

@Component({
  selector: 'app-collection-setting-dropdown',
  templateUrl: './setting-dropdow.component.html',
  styleUrls: ['./setting-dropdow.component.scss'],
  animations: [
    trigger('animation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('100ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [animate('100ms', style({ opacity: 0 }))]),
    ]),
  ],
})
export class CollectionSettingDropdownComponent implements OnInit, OnDestroy {
  private ngOnDestroy$ = new Subject();
  collection: Collection;

  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    combineLatest([this.route.params, this.store.select(selectCollections)])
      .pipe(
        takeUntil(this.ngOnDestroy$),
        map(([params, collections]) => ({
          collectionName: params.collectionName,
          collections,
        }))
      )
      .subscribe(({ collectionName, collections }) => {
        if (collectionName && collections) {
          this.collection = collections.find(
            (collectionEle) =>
              collectionEle.name.replace(/ /g, '').toLowerCase() ===
              collectionName
          );
        } else {
          this.collection = null;
        }
      });
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  onChange() {
    if (this.collection) {
      const dialogRef = this.dialog.open(CollectionDialogComponent, {
        width: '60vw',
        maxHeight: '80%',
      });

      dialogRef.componentInstance.mode = 'UPDATE';
      dialogRef.componentInstance.collection = this.collection;
    }
  }

  onDelete() {
    this.dialog.open(CollectionDeleteDialogComponent, {
      minWidth: '500px',
      data: this.collection,
    });
  }
}
