import { Directive, ElementRef, Input, OnInit } from '@angular/core';

import collectionTheme, { ColorTheme } from '@shared/collection-theme';

type CardsType = 'new' | 'review' | 'default';

@Directive({
  selector: '[appCardsAmount]',
})
export class CardsAmountDirective implements OnInit {
  @Input() colorCode: string;
  @Input('appCardsAmount') type: CardsType;

  constructor(private el: ElementRef) {}

  ngOnInit(): void {
    const colorName = Object.keys(collectionTheme).find(
      (key) =>
        (collectionTheme[key] as ColorTheme).background === this.colorCode
    );
    this.el.nativeElement.style.color = `#${
      (collectionTheme[colorName] as ColorTheme)[this.type]
    }`;
    this.el.nativeElement.style.fontWeight = 500;
    this.el.nativeElement.style.fontSize = '1rem';
  }
}
