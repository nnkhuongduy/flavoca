import { Router } from 'express';

import { isAuthenticated } from '../../middlewares/authenticated';
import Users from '../../models/user';
import { getUserClient } from '../../helpers/user';
import { errorHandler } from '../../helpers/global';

const authRouter = Router();

authRouter.get('/signin', isAuthenticated, async (req, res) => {
  if (req.session) {
    const id = req.session.passport.user;
    const user = await Users.findById(id);
    if (user) {
      res.json(getUserClient(user));
    } else {
      errorHandler(res, 'USER_NOT_FOUND', 404);
    }
  } else {
    errorHandler(res);
  }
});

authRouter.get('/signout', isAuthenticated, (req, res) => {
  if (req.session) {
    req.logOut();
    res.clearCookie('connect.sid');
    req.session.destroy((err) => {
      if (err) {
        errorHandler(res);
        return;
      } else {
      }
      return res.sendStatus(200);
    });
  } else {
    errorHandler(res);
  }
});

export default authRouter;
