import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@auth/auth-guard.service';
import { CardComponent } from '@card/card.component';

const routes: Routes = [
  { path: '', component: CardComponent, canActivate: [AuthGuard] },
  // { path: '', component: CardComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardRoutingModule {}
