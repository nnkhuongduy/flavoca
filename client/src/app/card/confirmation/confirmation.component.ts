import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './confirmation.component.html',
})
export class CardConfirmationComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: { content: string }) {}
}
