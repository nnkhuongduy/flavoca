import path from 'path';
require('dotenv').config({ path: path.join(__dirname, '../../.env') });
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import session, { SessionOptions } from 'express-session';
import connectMongo from 'connect-mongo';

import collectionsAPI from '../api/collections';
import cardsAPI from '../api/cards';
import testAPI from '../api/test';
import googleAuthAPI from '../api/auth/google';
import facebookAuthAPI from '../api/auth/facebook';
import oAuthAPI from '../api/auth/oauth';
import loginAPI from '../api/auth/auth';
import localAuthAPI from '../api/auth/local';
import passport from './passport';
import mongoose from './mongoose';

const app: express.Application = express();
const MongoStore = connectMongo(session);

const sess: SessionOptions = {
  secret: process.env.SESSION_SECRET as string,
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24,
    secure: false,
  },
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
};

if (app.get('env') === 'production') {
  app.set('trust proxy', 1);
  if (sess.cookie) {
    sess.cookie.secure = true;
  }
}

// tslint:disable-next-line: deprecation
app.use(bodyParser.json());
// tslint:disable-next-line: deprecation
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(session(sess));
app.use(passport.initialize());
app.use(passport.session());

app.use('/api', [collectionsAPI, testAPI, cardsAPI]);

app.use('/api/auth', [
  googleAuthAPI,
  facebookAuthAPI,
  localAuthAPI,
  oAuthAPI,
  loginAPI,
]);

export default app;
