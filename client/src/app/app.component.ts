import { Component, HostListener, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UtilitiesService } from './shared/services/utilities.service';
import { autoLogin } from './store/app.actions';
import { AppState } from './store/app.reducer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private store: Store<AppState>,
    private utilitiesSerivce: UtilitiesService
  ) {}

  ngOnInit(): void {
    // this.store.dispatch(autoLogin());
  }

  @HostListener('document:click', ['$event'])
  documentClick(event: any): void {
    this.utilitiesSerivce.documentClickedTarget.next(event.target);
  }
}
