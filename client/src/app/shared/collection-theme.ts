interface ColorBase {
  name: string;
  code: string;
}

export interface Color extends ColorBase {
  selected: boolean;
}

export interface ColorTheme {
  background: string;
  new: string;
  review: string;
  due: string;
}

type Theme = 'default' | 'red' | 'blue';

type CollectionTheme = {
  [theme in Theme]: ColorTheme;
};

const collectionTheme: CollectionTheme = {
  default: {
    background: 'd6d6d6',
    new: '2196f3',
    review: '4caf50',
    due: 'e53935',
  },
  red: {
    background: 'ff867c',
    new: '005cb2',
    review: '005b4f',
    due: '8e0038',
  },
  blue: {
    background: '6ec6ff',
    new: '6a0080',
    review: '255d00',
    due: '9a0007',
  },
};

export default collectionTheme;
