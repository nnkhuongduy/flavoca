import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, mergeMap, take, tap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import {
  newCollection,
  collectionRequestSuccess,
  collectionRequestFail,
  deleteCollection,
  updateCollection,
} from '@collection/store/collections.actions';
import { SnackbarComponent } from '@shared/components/snackbar/snackbar.component';
import { AppState } from '@app/store/app.reducer';
import { setCard, setCollections } from '@app/store/app.actions';
import { Collection } from '@collection/store/collections.reducer';
import { fetchCards } from '@card/store/card.actions';
import { CardService } from '@card/card.service';
import { selectCardInDic } from '@app/store/app.selectors';
import { CardInfo } from '@card/store/card.reducer';

@Injectable()
export class CollectionsEffect {
  constructor(
    private action$: Actions,
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router,
    private store: Store<AppState>,
    private cardService: CardService
  ) {}

  requestSuccess$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(collectionRequestSuccess),
        tap(({ snackbar }) => {
          if (snackbar) {
            this.snackBar.openFromComponent(SnackbarComponent, {
              data: snackbar,
              panelClass: ['snackbar', `snackbar-${snackbar.severity}`],
            });
          }
        })
      ),
    { dispatch: false }
  );

  requestFail$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(collectionRequestFail),
        tap(({ error }) => {
          this.snackBar.openFromComponent(SnackbarComponent, {
            data: {
              message: error,
              severity: 'error',
            },
            panelClass: ['snackbar', `snackbar-error`],
          });
        })
      ),
    { dispatch: false }
  );

  createCollection$ = createEffect(() =>
    this.action$.pipe(
      ofType(newCollection),
      exhaustMap((action) =>
        this.http.post<Collection[]>('/api/collections', action).pipe(
          mergeMap((collections) => {
            return [
              collectionRequestSuccess({
                snackbar: {
                  message: `Successfully create new ${action.name} collection!`,
                  severity: 'success',
                },
              }),
              setCollections({ collections }),
            ];
          }),
          catchError((error: HttpErrorResponse) =>
            of(collectionRequestFail({ error: error.statusText }))
          )
        )
      )
    )
  );

  deleteCollection$ = createEffect(() =>
    this.action$.pipe(
      ofType(deleteCollection),
      exhaustMap((action) =>
        this.http
          .put<Collection[]>('/api/collections', {
            action: 'DELETE',
            collectionId: action.collectionId,
            keepCards: action.keepCards,
          })
          .pipe(
            tap(() => {
              this.store.dispatch(fetchCards());
              const selectedId = this.cardService.selectedId.value;
              this.store
                .select(selectCardInDic, { cardId: selectedId })
                .pipe(take(1))
                .subscribe((card: CardInfo) => {
                  if (card && card.collectionId === action.collectionId) {
                    this.cardService.removeSelected();
                  }
                });
            }),
            mergeMap((collections) => {
              this.router.navigate(['/collections']);
              return [
                collectionRequestSuccess({
                  snackbar: {
                    message: `Successfully delete the collection!`,
                    severity: 'success',
                  },
                }),
                setCollections({ collections }),
              ];
            }),
            catchError((error: HttpErrorResponse) =>
              of(collectionRequestFail({ error: error.statusText }))
            )
          )
      )
    )
  );

  updateCollection$ = createEffect(() =>
    this.action$.pipe(
      ofType(updateCollection),
      exhaustMap(({ collectionId, update }) =>
        this.http
          .put<Collection[]>('/api/collections', {
            action: 'UPDATE',
            collectionId,
            update,
          })
          .pipe(
            mergeMap((collections) => {
              this.router.navigate(['/collections']);
              return [
                collectionRequestSuccess({
                  snackbar: {
                    message: `Successfully update the collection!`,
                    severity: 'success',
                  },
                }),
                setCollections({ collections }),
              ];
            }),
            catchError((error: HttpErrorResponse) =>
              of(collectionRequestFail({ error: error.statusText }))
            )
          )
      )
    )
  );
}
