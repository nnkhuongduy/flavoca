import { createFeatureSelector, createSelector } from '@ngrx/store';

import { AppState } from '@app/store/app.reducer';
import * as fromAuth from '@auth/store/auth.reducer';

export const selectAuth = createFeatureSelector<AppState, fromAuth.State>(
  fromAuth.authFeatureKey
);

export const selectIsSigning = createSelector(
  selectAuth,
  (state) => state.isSigning
);

export const selectErrorMessage = createSelector(
  selectAuth,
  (state) => state.errorMessage
);
