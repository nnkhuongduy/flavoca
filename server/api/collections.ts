import { Router } from 'express';
import { isAuthenticated } from '../middlewares/authenticated';
import {
  createCollection,
  getCollections,
  deleteCollection,
  updateCollection,
} from '../service/collection';

const collectionsRouter = Router();

collectionsRouter
  .route('/collections')
  .post(isAuthenticated, createCollection)
  .get(isAuthenticated, getCollections)
  .put(isAuthenticated, (req, res) => {
    const action = req.body.action;
    if (action === 'DELETE') {
      deleteCollection(req, res);
    }
    if (action == 'UPDATE') {
      updateCollection(req, res);
    }
  });

export default collectionsRouter;
