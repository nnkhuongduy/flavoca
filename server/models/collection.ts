import { Schema, model, Document } from 'mongoose';
import Cards, { CardStates } from './card';

export interface Collection extends Document {
  name: string;
  colorCode: string;
  createdAt: string;
  updatedAt: string;
  countCards: () => Promise<CountCards>;
}

export type CountCards = [number, number, number, number];

export const collectionSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      minlength: 1,
    },
    colorCode: {
      type: String,
      default: 'd6d6d6',
      required: true,
    },
  },
  { timestamps: { createdAt: true, updatedAt: true } }
);

collectionSchema.methods.countCards = async function (this: Collection) {
  try {
    const cards = await Cards.find({ collectionId: this._id });

    if (!cards) {
      throw Error('ERROR_DATABASE');
    }

    return cards.reduce<CountCards>(
      (currResult, currCard) => {
        const newResult: CountCards = [...currResult];
        newResult[currCard.state]++;
        return newResult;
      },
      [0, 0, 0, 0]
    );
  } catch (error) {
    throw Error(error.message);
  }
};

export default model<Collection>(
  'Collections',
  collectionSchema,
  'Collections'
);
