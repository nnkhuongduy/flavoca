import { NgModule } from '@angular/core';

import { BackdropDirective } from './backdrop.directive';
import { CardsAmountDirective } from './cards.directive';
import { DropdownDirective } from './dropdown.directive';

@NgModule({
  declarations: [DropdownDirective, BackdropDirective, CardsAmountDirective],
  exports: [DropdownDirective, BackdropDirective, CardsAmountDirective],
})
export class DirectiveModule {}
